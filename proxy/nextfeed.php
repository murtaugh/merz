<?php
// PROXY TO LOAD DATA FROM LIVE API
$handle = fopen("https://portal.merz-akademie.de/dep/api/nextfeed", "rb");
if (FALSE === $handle) {
    exit("Failed to open stream to URL");
}

$contents = '';
while (!feof($handle)) {
    $contents .= fread($handle, 8192);
}
fclose($handle);

header('Content-Type: application/json');
echo $contents;
