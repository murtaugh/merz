API
========


Index
----------------------

<https://portal.merz-akademie.de/dep/api/nextfeed>


event_category
? professor ?
Add links view texts?
LINKS campusnet


Links in
-------------
Yeah thats possible:  
German Version of he campusnet course detail site:  
$cn_lang_param = '-N000000000000001';

Engish Version of the campusnet course detail site:  
$cn_lang_param = '-N000000000000002'; 

$item->course_id is the course ID from the API feed.

$url = 'https://campusnet.merz-akademie.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=COURSEDETAILS&ARGUMENTS=' . $cn_lang_param . ',-N000304,-N000000000000000,-N' . $item->course_id . ',-N' . $item->course_id;

Example urls: 

* <https://campusnet.merz-akademie.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=COURSEDETAILS&ARGUMENTS=-N000000000000001,-N000304,-N000000000000000,-N372283898931963,-N372283898931963>
* <https://campusnet.merz-akademie.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=COURSEDETAILS&ARGUMENTS=-N000000000000002,-N000304,-N000000000000000,-N372283898931963,-N372283898931963> 

(course_id = 372283898931963, N000000000000001 for german, N000000000000002 for english version)


Course Description
----------------------
New api function: 

courseInfos (also live data with 10 minutes caching)
e.g. for course with id 372283898931963:

* JSON: <https://portal.merz-akademie.de/dep/api/courseInfos?course_id=372283898931963>
* XML: <https://portal.merz-akademie.de/dep/api/courseInfos?course_id=372283898931963&output=xml>

Output fields are:

Name
: Name of the text field. E.g. "Inhalte und Ziele", "Bewertungskritierien". Kind of types/headlines for the text.

Text
: The full text

Module
--------------
Campusnet: Modulinformationen zur jeweiligen Lehrveranstaltung *Module information for the respective course*

Lehrveranstaltungen können in beliebig vielen Lehrmodulen enthalten sein, und können über die API-Funktion courseModuleInfos abgefragt werden.

*Courses can be included in any number of course modules and can be queried via the API function courseModuleInfos.*

GET-Request auf: <https://portal.merz-akademie.de/dep/api/courseModuleInfos>

Möglliche Parameter:

output
:    json (Standardwert) oder xml

course_id
:   Dieser Parameter ist zwingend erforderlich. Die course_id wird von der nextfeed-Funktion 
zurückgegeben.

Ausgabe: Daten werden, je nach Parameter, als XML-Feed oder JSON zurückgegeben. Es sind alle Felder aus dem Campusnet verfügbar.


* JSON: 
<https://portal.merz-akademie.de/dep/api/courseModuleInfos?course_id=372283898931963>
* XML: <https://portal.merz-akademie.de/dep/api/courseModuleInfos?course_id=372283898931963&output=xml>

