Proof of concept interface Merz Akademie
(c) 2019 Michael Murtaugh
Released under [GPL3 license](https://www.gnu.org/licenses/gpl-3.0.en.html). See LICENSE.txt


To distribute
---------------------
Files to copy

* index.html
* style.css
* dist/index.js


To (re)build
-------------

npm install
make

