all: dist/index.js

dist/index.js: src/*.js
	node_modules/.bin/rollup -c

static/nextfeed.json:
	wget https://portal.merz-akademie.de/dep/api/nextfeed -O $@