var index = (function (exports) {
  'use strict';

  var xhtml = "http://www.w3.org/1999/xhtml";

  var namespaces = {
    svg: "http://www.w3.org/2000/svg",
    xhtml: xhtml,
    xlink: "http://www.w3.org/1999/xlink",
    xml: "http://www.w3.org/XML/1998/namespace",
    xmlns: "http://www.w3.org/2000/xmlns/"
  };

  function namespace(name) {
    var prefix = name += "", i = prefix.indexOf(":");
    if (i >= 0 && (prefix = name.slice(0, i)) !== "xmlns") name = name.slice(i + 1);
    return namespaces.hasOwnProperty(prefix) ? {space: namespaces[prefix], local: name} : name;
  }

  function creatorInherit(name) {
    return function() {
      var document = this.ownerDocument,
          uri = this.namespaceURI;
      return uri === xhtml && document.documentElement.namespaceURI === xhtml
          ? document.createElement(name)
          : document.createElementNS(uri, name);
    };
  }

  function creatorFixed(fullname) {
    return function() {
      return this.ownerDocument.createElementNS(fullname.space, fullname.local);
    };
  }

  function creator(name) {
    var fullname = namespace(name);
    return (fullname.local
        ? creatorFixed
        : creatorInherit)(fullname);
  }

  function none() {}

  function selector(selector) {
    return selector == null ? none : function() {
      return this.querySelector(selector);
    };
  }

  function selection_select(select) {
    if (typeof select !== "function") select = selector(select);

    for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, subgroup = subgroups[j] = new Array(n), node, subnode, i = 0; i < n; ++i) {
        if ((node = group[i]) && (subnode = select.call(node, node.__data__, i, group))) {
          if ("__data__" in node) subnode.__data__ = node.__data__;
          subgroup[i] = subnode;
        }
      }
    }

    return new Selection(subgroups, this._parents);
  }

  function empty() {
    return [];
  }

  function selectorAll(selector) {
    return selector == null ? empty : function() {
      return this.querySelectorAll(selector);
    };
  }

  function selection_selectAll(select) {
    if (typeof select !== "function") select = selectorAll(select);

    for (var groups = this._groups, m = groups.length, subgroups = [], parents = [], j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
        if (node = group[i]) {
          subgroups.push(select.call(node, node.__data__, i, group));
          parents.push(node);
        }
      }
    }

    return new Selection(subgroups, parents);
  }

  function matcher(selector) {
    return function() {
      return this.matches(selector);
    };
  }

  function selection_filter(match) {
    if (typeof match !== "function") match = matcher(match);

    for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, subgroup = subgroups[j] = [], node, i = 0; i < n; ++i) {
        if ((node = group[i]) && match.call(node, node.__data__, i, group)) {
          subgroup.push(node);
        }
      }
    }

    return new Selection(subgroups, this._parents);
  }

  function sparse(update) {
    return new Array(update.length);
  }

  function selection_enter() {
    return new Selection(this._enter || this._groups.map(sparse), this._parents);
  }

  function EnterNode(parent, datum) {
    this.ownerDocument = parent.ownerDocument;
    this.namespaceURI = parent.namespaceURI;
    this._next = null;
    this._parent = parent;
    this.__data__ = datum;
  }

  EnterNode.prototype = {
    constructor: EnterNode,
    appendChild: function(child) { return this._parent.insertBefore(child, this._next); },
    insertBefore: function(child, next) { return this._parent.insertBefore(child, next); },
    querySelector: function(selector) { return this._parent.querySelector(selector); },
    querySelectorAll: function(selector) { return this._parent.querySelectorAll(selector); }
  };

  function constant(x) {
    return function() {
      return x;
    };
  }

  var keyPrefix = "$"; // Protect against keys like “__proto__”.

  function bindIndex(parent, group, enter, update, exit, data) {
    var i = 0,
        node,
        groupLength = group.length,
        dataLength = data.length;

    // Put any non-null nodes that fit into update.
    // Put any null nodes into enter.
    // Put any remaining data into enter.
    for (; i < dataLength; ++i) {
      if (node = group[i]) {
        node.__data__ = data[i];
        update[i] = node;
      } else {
        enter[i] = new EnterNode(parent, data[i]);
      }
    }

    // Put any non-null nodes that don’t fit into exit.
    for (; i < groupLength; ++i) {
      if (node = group[i]) {
        exit[i] = node;
      }
    }
  }

  function bindKey(parent, group, enter, update, exit, data, key) {
    var i,
        node,
        nodeByKeyValue = {},
        groupLength = group.length,
        dataLength = data.length,
        keyValues = new Array(groupLength),
        keyValue;

    // Compute the key for each node.
    // If multiple nodes have the same key, the duplicates are added to exit.
    for (i = 0; i < groupLength; ++i) {
      if (node = group[i]) {
        keyValues[i] = keyValue = keyPrefix + key.call(node, node.__data__, i, group);
        if (keyValue in nodeByKeyValue) {
          exit[i] = node;
        } else {
          nodeByKeyValue[keyValue] = node;
        }
      }
    }

    // Compute the key for each datum.
    // If there a node associated with this key, join and add it to update.
    // If there is not (or the key is a duplicate), add it to enter.
    for (i = 0; i < dataLength; ++i) {
      keyValue = keyPrefix + key.call(parent, data[i], i, data);
      if (node = nodeByKeyValue[keyValue]) {
        update[i] = node;
        node.__data__ = data[i];
        nodeByKeyValue[keyValue] = null;
      } else {
        enter[i] = new EnterNode(parent, data[i]);
      }
    }

    // Add any remaining nodes that were not bound to data to exit.
    for (i = 0; i < groupLength; ++i) {
      if ((node = group[i]) && (nodeByKeyValue[keyValues[i]] === node)) {
        exit[i] = node;
      }
    }
  }

  function selection_data(value, key) {
    if (!value) {
      data = new Array(this.size()), j = -1;
      this.each(function(d) { data[++j] = d; });
      return data;
    }

    var bind = key ? bindKey : bindIndex,
        parents = this._parents,
        groups = this._groups;

    if (typeof value !== "function") value = constant(value);

    for (var m = groups.length, update = new Array(m), enter = new Array(m), exit = new Array(m), j = 0; j < m; ++j) {
      var parent = parents[j],
          group = groups[j],
          groupLength = group.length,
          data = value.call(parent, parent && parent.__data__, j, parents),
          dataLength = data.length,
          enterGroup = enter[j] = new Array(dataLength),
          updateGroup = update[j] = new Array(dataLength),
          exitGroup = exit[j] = new Array(groupLength);

      bind(parent, group, enterGroup, updateGroup, exitGroup, data, key);

      // Now connect the enter nodes to their following update node, such that
      // appendChild can insert the materialized enter node before this node,
      // rather than at the end of the parent node.
      for (var i0 = 0, i1 = 0, previous, next; i0 < dataLength; ++i0) {
        if (previous = enterGroup[i0]) {
          if (i0 >= i1) i1 = i0 + 1;
          while (!(next = updateGroup[i1]) && ++i1 < dataLength);
          previous._next = next || null;
        }
      }
    }

    update = new Selection(update, parents);
    update._enter = enter;
    update._exit = exit;
    return update;
  }

  function selection_exit() {
    return new Selection(this._exit || this._groups.map(sparse), this._parents);
  }

  function selection_join(onenter, onupdate, onexit) {
    var enter = this.enter(), update = this, exit = this.exit();
    enter = typeof onenter === "function" ? onenter(enter) : enter.append(onenter + "");
    if (onupdate != null) update = onupdate(update);
    if (onexit == null) exit.remove(); else onexit(exit);
    return enter && update ? enter.merge(update).order() : update;
  }

  function selection_merge(selection) {

    for (var groups0 = this._groups, groups1 = selection._groups, m0 = groups0.length, m1 = groups1.length, m = Math.min(m0, m1), merges = new Array(m0), j = 0; j < m; ++j) {
      for (var group0 = groups0[j], group1 = groups1[j], n = group0.length, merge = merges[j] = new Array(n), node, i = 0; i < n; ++i) {
        if (node = group0[i] || group1[i]) {
          merge[i] = node;
        }
      }
    }

    for (; j < m0; ++j) {
      merges[j] = groups0[j];
    }

    return new Selection(merges, this._parents);
  }

  function selection_order() {

    for (var groups = this._groups, j = -1, m = groups.length; ++j < m;) {
      for (var group = groups[j], i = group.length - 1, next = group[i], node; --i >= 0;) {
        if (node = group[i]) {
          if (next && node.compareDocumentPosition(next) ^ 4) next.parentNode.insertBefore(node, next);
          next = node;
        }
      }
    }

    return this;
  }

  function selection_sort(compare) {
    if (!compare) compare = ascending;

    function compareNode(a, b) {
      return a && b ? compare(a.__data__, b.__data__) : !a - !b;
    }

    for (var groups = this._groups, m = groups.length, sortgroups = new Array(m), j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, sortgroup = sortgroups[j] = new Array(n), node, i = 0; i < n; ++i) {
        if (node = group[i]) {
          sortgroup[i] = node;
        }
      }
      sortgroup.sort(compareNode);
    }

    return new Selection(sortgroups, this._parents).order();
  }

  function ascending(a, b) {
    return a < b ? -1 : a > b ? 1 : a >= b ? 0 : NaN;
  }

  function selection_call() {
    var callback = arguments[0];
    arguments[0] = this;
    callback.apply(null, arguments);
    return this;
  }

  function selection_nodes() {
    var nodes = new Array(this.size()), i = -1;
    this.each(function() { nodes[++i] = this; });
    return nodes;
  }

  function selection_node() {

    for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
      for (var group = groups[j], i = 0, n = group.length; i < n; ++i) {
        var node = group[i];
        if (node) return node;
      }
    }

    return null;
  }

  function selection_size() {
    var size = 0;
    this.each(function() { ++size; });
    return size;
  }

  function selection_empty() {
    return !this.node();
  }

  function selection_each(callback) {

    for (var groups = this._groups, j = 0, m = groups.length; j < m; ++j) {
      for (var group = groups[j], i = 0, n = group.length, node; i < n; ++i) {
        if (node = group[i]) callback.call(node, node.__data__, i, group);
      }
    }

    return this;
  }

  function attrRemove(name) {
    return function() {
      this.removeAttribute(name);
    };
  }

  function attrRemoveNS(fullname) {
    return function() {
      this.removeAttributeNS(fullname.space, fullname.local);
    };
  }

  function attrConstant(name, value) {
    return function() {
      this.setAttribute(name, value);
    };
  }

  function attrConstantNS(fullname, value) {
    return function() {
      this.setAttributeNS(fullname.space, fullname.local, value);
    };
  }

  function attrFunction(name, value) {
    return function() {
      var v = value.apply(this, arguments);
      if (v == null) this.removeAttribute(name);
      else this.setAttribute(name, v);
    };
  }

  function attrFunctionNS(fullname, value) {
    return function() {
      var v = value.apply(this, arguments);
      if (v == null) this.removeAttributeNS(fullname.space, fullname.local);
      else this.setAttributeNS(fullname.space, fullname.local, v);
    };
  }

  function selection_attr(name, value) {
    var fullname = namespace(name);

    if (arguments.length < 2) {
      var node = this.node();
      return fullname.local
          ? node.getAttributeNS(fullname.space, fullname.local)
          : node.getAttribute(fullname);
    }

    return this.each((value == null
        ? (fullname.local ? attrRemoveNS : attrRemove) : (typeof value === "function"
        ? (fullname.local ? attrFunctionNS : attrFunction)
        : (fullname.local ? attrConstantNS : attrConstant)))(fullname, value));
  }

  function defaultView(node) {
    return (node.ownerDocument && node.ownerDocument.defaultView) // node is a Node
        || (node.document && node) // node is a Window
        || node.defaultView; // node is a Document
  }

  function styleRemove(name) {
    return function() {
      this.style.removeProperty(name);
    };
  }

  function styleConstant(name, value, priority) {
    return function() {
      this.style.setProperty(name, value, priority);
    };
  }

  function styleFunction(name, value, priority) {
    return function() {
      var v = value.apply(this, arguments);
      if (v == null) this.style.removeProperty(name);
      else this.style.setProperty(name, v, priority);
    };
  }

  function selection_style(name, value, priority) {
    return arguments.length > 1
        ? this.each((value == null
              ? styleRemove : typeof value === "function"
              ? styleFunction
              : styleConstant)(name, value, priority == null ? "" : priority))
        : styleValue(this.node(), name);
  }

  function styleValue(node, name) {
    return node.style.getPropertyValue(name)
        || defaultView(node).getComputedStyle(node, null).getPropertyValue(name);
  }

  function propertyRemove(name) {
    return function() {
      delete this[name];
    };
  }

  function propertyConstant(name, value) {
    return function() {
      this[name] = value;
    };
  }

  function propertyFunction(name, value) {
    return function() {
      var v = value.apply(this, arguments);
      if (v == null) delete this[name];
      else this[name] = v;
    };
  }

  function selection_property(name, value) {
    return arguments.length > 1
        ? this.each((value == null
            ? propertyRemove : typeof value === "function"
            ? propertyFunction
            : propertyConstant)(name, value))
        : this.node()[name];
  }

  function classArray(string) {
    return string.trim().split(/^|\s+/);
  }

  function classList(node) {
    return node.classList || new ClassList(node);
  }

  function ClassList(node) {
    this._node = node;
    this._names = classArray(node.getAttribute("class") || "");
  }

  ClassList.prototype = {
    add: function(name) {
      var i = this._names.indexOf(name);
      if (i < 0) {
        this._names.push(name);
        this._node.setAttribute("class", this._names.join(" "));
      }
    },
    remove: function(name) {
      var i = this._names.indexOf(name);
      if (i >= 0) {
        this._names.splice(i, 1);
        this._node.setAttribute("class", this._names.join(" "));
      }
    },
    contains: function(name) {
      return this._names.indexOf(name) >= 0;
    }
  };

  function classedAdd(node, names) {
    var list = classList(node), i = -1, n = names.length;
    while (++i < n) list.add(names[i]);
  }

  function classedRemove(node, names) {
    var list = classList(node), i = -1, n = names.length;
    while (++i < n) list.remove(names[i]);
  }

  function classedTrue(names) {
    return function() {
      classedAdd(this, names);
    };
  }

  function classedFalse(names) {
    return function() {
      classedRemove(this, names);
    };
  }

  function classedFunction(names, value) {
    return function() {
      (value.apply(this, arguments) ? classedAdd : classedRemove)(this, names);
    };
  }

  function selection_classed(name, value) {
    var names = classArray(name + "");

    if (arguments.length < 2) {
      var list = classList(this.node()), i = -1, n = names.length;
      while (++i < n) if (!list.contains(names[i])) return false;
      return true;
    }

    return this.each((typeof value === "function"
        ? classedFunction : value
        ? classedTrue
        : classedFalse)(names, value));
  }

  function textRemove() {
    this.textContent = "";
  }

  function textConstant(value) {
    return function() {
      this.textContent = value;
    };
  }

  function textFunction(value) {
    return function() {
      var v = value.apply(this, arguments);
      this.textContent = v == null ? "" : v;
    };
  }

  function selection_text(value) {
    return arguments.length
        ? this.each(value == null
            ? textRemove : (typeof value === "function"
            ? textFunction
            : textConstant)(value))
        : this.node().textContent;
  }

  function htmlRemove() {
    this.innerHTML = "";
  }

  function htmlConstant(value) {
    return function() {
      this.innerHTML = value;
    };
  }

  function htmlFunction(value) {
    return function() {
      var v = value.apply(this, arguments);
      this.innerHTML = v == null ? "" : v;
    };
  }

  function selection_html(value) {
    return arguments.length
        ? this.each(value == null
            ? htmlRemove : (typeof value === "function"
            ? htmlFunction
            : htmlConstant)(value))
        : this.node().innerHTML;
  }

  function raise() {
    if (this.nextSibling) this.parentNode.appendChild(this);
  }

  function selection_raise() {
    return this.each(raise);
  }

  function lower() {
    if (this.previousSibling) this.parentNode.insertBefore(this, this.parentNode.firstChild);
  }

  function selection_lower() {
    return this.each(lower);
  }

  function selection_append(name) {
    var create = typeof name === "function" ? name : creator(name);
    return this.select(function() {
      return this.appendChild(create.apply(this, arguments));
    });
  }

  function constantNull() {
    return null;
  }

  function selection_insert(name, before) {
    var create = typeof name === "function" ? name : creator(name),
        select = before == null ? constantNull : typeof before === "function" ? before : selector(before);
    return this.select(function() {
      return this.insertBefore(create.apply(this, arguments), select.apply(this, arguments) || null);
    });
  }

  function remove() {
    var parent = this.parentNode;
    if (parent) parent.removeChild(this);
  }

  function selection_remove() {
    return this.each(remove);
  }

  function selection_cloneShallow() {
    return this.parentNode.insertBefore(this.cloneNode(false), this.nextSibling);
  }

  function selection_cloneDeep() {
    return this.parentNode.insertBefore(this.cloneNode(true), this.nextSibling);
  }

  function selection_clone(deep) {
    return this.select(deep ? selection_cloneDeep : selection_cloneShallow);
  }

  function selection_datum(value) {
    return arguments.length
        ? this.property("__data__", value)
        : this.node().__data__;
  }

  var filterEvents = {};

  var event = null;

  if (typeof document !== "undefined") {
    var element = document.documentElement;
    if (!("onmouseenter" in element)) {
      filterEvents = {mouseenter: "mouseover", mouseleave: "mouseout"};
    }
  }

  function filterContextListener(listener, index, group) {
    listener = contextListener(listener, index, group);
    return function(event) {
      var related = event.relatedTarget;
      if (!related || (related !== this && !(related.compareDocumentPosition(this) & 8))) {
        listener.call(this, event);
      }
    };
  }

  function contextListener(listener, index, group) {
    return function(event1) {
      var event0 = event; // Events can be reentrant (e.g., focus).
      event = event1;
      try {
        listener.call(this, this.__data__, index, group);
      } finally {
        event = event0;
      }
    };
  }

  function parseTypenames(typenames) {
    return typenames.trim().split(/^|\s+/).map(function(t) {
      var name = "", i = t.indexOf(".");
      if (i >= 0) name = t.slice(i + 1), t = t.slice(0, i);
      return {type: t, name: name};
    });
  }

  function onRemove(typename) {
    return function() {
      var on = this.__on;
      if (!on) return;
      for (var j = 0, i = -1, m = on.length, o; j < m; ++j) {
        if (o = on[j], (!typename.type || o.type === typename.type) && o.name === typename.name) {
          this.removeEventListener(o.type, o.listener, o.capture);
        } else {
          on[++i] = o;
        }
      }
      if (++i) on.length = i;
      else delete this.__on;
    };
  }

  function onAdd(typename, value, capture) {
    var wrap = filterEvents.hasOwnProperty(typename.type) ? filterContextListener : contextListener;
    return function(d, i, group) {
      var on = this.__on, o, listener = wrap(value, i, group);
      if (on) for (var j = 0, m = on.length; j < m; ++j) {
        if ((o = on[j]).type === typename.type && o.name === typename.name) {
          this.removeEventListener(o.type, o.listener, o.capture);
          this.addEventListener(o.type, o.listener = listener, o.capture = capture);
          o.value = value;
          return;
        }
      }
      this.addEventListener(typename.type, listener, capture);
      o = {type: typename.type, name: typename.name, value: value, listener: listener, capture: capture};
      if (!on) this.__on = [o];
      else on.push(o);
    };
  }

  function selection_on(typename, value, capture) {
    var typenames = parseTypenames(typename + ""), i, n = typenames.length, t;

    if (arguments.length < 2) {
      var on = this.node().__on;
      if (on) for (var j = 0, m = on.length, o; j < m; ++j) {
        for (i = 0, o = on[j]; i < n; ++i) {
          if ((t = typenames[i]).type === o.type && t.name === o.name) {
            return o.value;
          }
        }
      }
      return;
    }

    on = value ? onAdd : onRemove;
    if (capture == null) capture = false;
    for (i = 0; i < n; ++i) this.each(on(typenames[i], value, capture));
    return this;
  }

  function dispatchEvent(node, type, params) {
    var window = defaultView(node),
        event = window.CustomEvent;

    if (typeof event === "function") {
      event = new event(type, params);
    } else {
      event = window.document.createEvent("Event");
      if (params) event.initEvent(type, params.bubbles, params.cancelable), event.detail = params.detail;
      else event.initEvent(type, false, false);
    }

    node.dispatchEvent(event);
  }

  function dispatchConstant(type, params) {
    return function() {
      return dispatchEvent(this, type, params);
    };
  }

  function dispatchFunction(type, params) {
    return function() {
      return dispatchEvent(this, type, params.apply(this, arguments));
    };
  }

  function selection_dispatch(type, params) {
    return this.each((typeof params === "function"
        ? dispatchFunction
        : dispatchConstant)(type, params));
  }

  var root = [null];

  function Selection(groups, parents) {
    this._groups = groups;
    this._parents = parents;
  }

  function selection() {
    return new Selection([[document.documentElement]], root);
  }

  Selection.prototype = selection.prototype = {
    constructor: Selection,
    select: selection_select,
    selectAll: selection_selectAll,
    filter: selection_filter,
    data: selection_data,
    enter: selection_enter,
    exit: selection_exit,
    join: selection_join,
    merge: selection_merge,
    order: selection_order,
    sort: selection_sort,
    call: selection_call,
    nodes: selection_nodes,
    node: selection_node,
    size: selection_size,
    empty: selection_empty,
    each: selection_each,
    attr: selection_attr,
    style: selection_style,
    property: selection_property,
    classed: selection_classed,
    text: selection_text,
    html: selection_html,
    raise: selection_raise,
    lower: selection_lower,
    append: selection_append,
    insert: selection_insert,
    remove: selection_remove,
    clone: selection_clone,
    datum: selection_datum,
    on: selection_on,
    dispatch: selection_dispatch
  };

  function select(selector) {
    return typeof selector === "string"
        ? new Selection([[document.querySelector(selector)]], [document.documentElement])
        : new Selection([[selector]], root);
  }

  var noop = {value: function() {}};

  function dispatch() {
    for (var i = 0, n = arguments.length, _ = {}, t; i < n; ++i) {
      if (!(t = arguments[i] + "") || (t in _)) throw new Error("illegal type: " + t);
      _[t] = [];
    }
    return new Dispatch(_);
  }

  function Dispatch(_) {
    this._ = _;
  }

  function parseTypenames$1(typenames, types) {
    return typenames.trim().split(/^|\s+/).map(function(t) {
      var name = "", i = t.indexOf(".");
      if (i >= 0) name = t.slice(i + 1), t = t.slice(0, i);
      if (t && !types.hasOwnProperty(t)) throw new Error("unknown type: " + t);
      return {type: t, name: name};
    });
  }

  Dispatch.prototype = dispatch.prototype = {
    constructor: Dispatch,
    on: function(typename, callback) {
      var _ = this._,
          T = parseTypenames$1(typename + "", _),
          t,
          i = -1,
          n = T.length;

      // If no callback was specified, return the callback of the given type and name.
      if (arguments.length < 2) {
        while (++i < n) if ((t = (typename = T[i]).type) && (t = get(_[t], typename.name))) return t;
        return;
      }

      // If a type was specified, set the callback for the given type and name.
      // Otherwise, if a null callback was specified, remove callbacks of the given name.
      if (callback != null && typeof callback !== "function") throw new Error("invalid callback: " + callback);
      while (++i < n) {
        if (t = (typename = T[i]).type) _[t] = set(_[t], typename.name, callback);
        else if (callback == null) for (t in _) _[t] = set(_[t], typename.name, null);
      }

      return this;
    },
    copy: function() {
      var copy = {}, _ = this._;
      for (var t in _) copy[t] = _[t].slice();
      return new Dispatch(copy);
    },
    call: function(type, that) {
      if ((n = arguments.length - 2) > 0) for (var args = new Array(n), i = 0, n, t; i < n; ++i) args[i] = arguments[i + 2];
      if (!this._.hasOwnProperty(type)) throw new Error("unknown type: " + type);
      for (t = this._[type], i = 0, n = t.length; i < n; ++i) t[i].value.apply(that, args);
    },
    apply: function(type, that, args) {
      if (!this._.hasOwnProperty(type)) throw new Error("unknown type: " + type);
      for (var t = this._[type], i = 0, n = t.length; i < n; ++i) t[i].value.apply(that, args);
    }
  };

  function get(type, name) {
    for (var i = 0, n = type.length, c; i < n; ++i) {
      if ((c = type[i]).name === name) {
        return c.value;
      }
    }
  }

  function set(type, name, callback) {
    for (var i = 0, n = type.length; i < n; ++i) {
      if (type[i].name === name) {
        type[i] = noop, type = type.slice(0, i).concat(type.slice(i + 1));
        break;
      }
    }
    if (callback != null) type.push({name: name, value: callback});
    return type;
  }

  var frame = 0, // is an animation frame pending?
      timeout = 0, // is a timeout pending?
      interval = 0, // are any timers active?
      pokeDelay = 1000, // how frequently we check for clock skew
      taskHead,
      taskTail,
      clockLast = 0,
      clockNow = 0,
      clockSkew = 0,
      clock = typeof performance === "object" && performance.now ? performance : Date,
      setFrame = typeof window === "object" && window.requestAnimationFrame ? window.requestAnimationFrame.bind(window) : function(f) { setTimeout(f, 17); };

  function now() {
    return clockNow || (setFrame(clearNow), clockNow = clock.now() + clockSkew);
  }

  function clearNow() {
    clockNow = 0;
  }

  function Timer() {
    this._call =
    this._time =
    this._next = null;
  }

  Timer.prototype = timer.prototype = {
    constructor: Timer,
    restart: function(callback, delay, time) {
      if (typeof callback !== "function") throw new TypeError("callback is not a function");
      time = (time == null ? now() : +time) + (delay == null ? 0 : +delay);
      if (!this._next && taskTail !== this) {
        if (taskTail) taskTail._next = this;
        else taskHead = this;
        taskTail = this;
      }
      this._call = callback;
      this._time = time;
      sleep();
    },
    stop: function() {
      if (this._call) {
        this._call = null;
        this._time = Infinity;
        sleep();
      }
    }
  };

  function timer(callback, delay, time) {
    var t = new Timer;
    t.restart(callback, delay, time);
    return t;
  }

  function timerFlush() {
    now(); // Get the current time, if not already set.
    ++frame; // Pretend we’ve set an alarm, if we haven’t already.
    var t = taskHead, e;
    while (t) {
      if ((e = clockNow - t._time) >= 0) t._call.call(null, e);
      t = t._next;
    }
    --frame;
  }

  function wake() {
    clockNow = (clockLast = clock.now()) + clockSkew;
    frame = timeout = 0;
    try {
      timerFlush();
    } finally {
      frame = 0;
      nap();
      clockNow = 0;
    }
  }

  function poke() {
    var now = clock.now(), delay = now - clockLast;
    if (delay > pokeDelay) clockSkew -= delay, clockLast = now;
  }

  function nap() {
    var t0, t1 = taskHead, t2, time = Infinity;
    while (t1) {
      if (t1._call) {
        if (time > t1._time) time = t1._time;
        t0 = t1, t1 = t1._next;
      } else {
        t2 = t1._next, t1._next = null;
        t1 = t0 ? t0._next = t2 : taskHead = t2;
      }
    }
    taskTail = t0;
    sleep(time);
  }

  function sleep(time) {
    if (frame) return; // Soonest alarm already set, or will be.
    if (timeout) timeout = clearTimeout(timeout);
    var delay = time - clockNow; // Strictly less than if we recomputed clockNow.
    if (delay > 24) {
      if (time < Infinity) timeout = setTimeout(wake, time - clock.now() - clockSkew);
      if (interval) interval = clearInterval(interval);
    } else {
      if (!interval) clockLast = clock.now(), interval = setInterval(poke, pokeDelay);
      frame = 1, setFrame(wake);
    }
  }

  function timeout$1(callback, delay, time) {
    var t = new Timer;
    delay = delay == null ? 0 : +delay;
    t.restart(function(elapsed) {
      t.stop();
      callback(elapsed + delay);
    }, delay, time);
    return t;
  }

  var emptyOn = dispatch("start", "end", "cancel", "interrupt");
  var emptyTween = [];

  var CREATED = 0;
  var SCHEDULED = 1;
  var STARTING = 2;
  var STARTED = 3;
  var RUNNING = 4;
  var ENDING = 5;
  var ENDED = 6;

  function schedule(node, name, id, index, group, timing) {
    var schedules = node.__transition;
    if (!schedules) node.__transition = {};
    else if (id in schedules) return;
    create(node, id, {
      name: name,
      index: index, // For context during callback.
      group: group, // For context during callback.
      on: emptyOn,
      tween: emptyTween,
      time: timing.time,
      delay: timing.delay,
      duration: timing.duration,
      ease: timing.ease,
      timer: null,
      state: CREATED
    });
  }

  function init(node, id) {
    var schedule = get$1(node, id);
    if (schedule.state > CREATED) throw new Error("too late; already scheduled");
    return schedule;
  }

  function set$1(node, id) {
    var schedule = get$1(node, id);
    if (schedule.state > STARTED) throw new Error("too late; already running");
    return schedule;
  }

  function get$1(node, id) {
    var schedule = node.__transition;
    if (!schedule || !(schedule = schedule[id])) throw new Error("transition not found");
    return schedule;
  }

  function create(node, id, self) {
    var schedules = node.__transition,
        tween;

    // Initialize the self timer when the transition is created.
    // Note the actual delay is not known until the first callback!
    schedules[id] = self;
    self.timer = timer(schedule, 0, self.time);

    function schedule(elapsed) {
      self.state = SCHEDULED;
      self.timer.restart(start, self.delay, self.time);

      // If the elapsed delay is less than our first sleep, start immediately.
      if (self.delay <= elapsed) start(elapsed - self.delay);
    }

    function start(elapsed) {
      var i, j, n, o;

      // If the state is not SCHEDULED, then we previously errored on start.
      if (self.state !== SCHEDULED) return stop();

      for (i in schedules) {
        o = schedules[i];
        if (o.name !== self.name) continue;

        // While this element already has a starting transition during this frame,
        // defer starting an interrupting transition until that transition has a
        // chance to tick (and possibly end); see d3/d3-transition#54!
        if (o.state === STARTED) return timeout$1(start);

        // Interrupt the active transition, if any.
        if (o.state === RUNNING) {
          o.state = ENDED;
          o.timer.stop();
          o.on.call("interrupt", node, node.__data__, o.index, o.group);
          delete schedules[i];
        }

        // Cancel any pre-empted transitions.
        else if (+i < id) {
          o.state = ENDED;
          o.timer.stop();
          o.on.call("cancel", node, node.__data__, o.index, o.group);
          delete schedules[i];
        }
      }

      // Defer the first tick to end of the current frame; see d3/d3#1576.
      // Note the transition may be canceled after start and before the first tick!
      // Note this must be scheduled before the start event; see d3/d3-transition#16!
      // Assuming this is successful, subsequent callbacks go straight to tick.
      timeout$1(function() {
        if (self.state === STARTED) {
          self.state = RUNNING;
          self.timer.restart(tick, self.delay, self.time);
          tick(elapsed);
        }
      });

      // Dispatch the start event.
      // Note this must be done before the tween are initialized.
      self.state = STARTING;
      self.on.call("start", node, node.__data__, self.index, self.group);
      if (self.state !== STARTING) return; // interrupted
      self.state = STARTED;

      // Initialize the tween, deleting null tween.
      tween = new Array(n = self.tween.length);
      for (i = 0, j = -1; i < n; ++i) {
        if (o = self.tween[i].value.call(node, node.__data__, self.index, self.group)) {
          tween[++j] = o;
        }
      }
      tween.length = j + 1;
    }

    function tick(elapsed) {
      var t = elapsed < self.duration ? self.ease.call(null, elapsed / self.duration) : (self.timer.restart(stop), self.state = ENDING, 1),
          i = -1,
          n = tween.length;

      while (++i < n) {
        tween[i].call(node, t);
      }

      // Dispatch the end event.
      if (self.state === ENDING) {
        self.on.call("end", node, node.__data__, self.index, self.group);
        stop();
      }
    }

    function stop() {
      self.state = ENDED;
      self.timer.stop();
      delete schedules[id];
      for (var i in schedules) return; // eslint-disable-line no-unused-vars
      delete node.__transition;
    }
  }

  function interrupt(node, name) {
    var schedules = node.__transition,
        schedule,
        active,
        empty = true,
        i;

    if (!schedules) return;

    name = name == null ? null : name + "";

    for (i in schedules) {
      if ((schedule = schedules[i]).name !== name) { empty = false; continue; }
      active = schedule.state > STARTING && schedule.state < ENDING;
      schedule.state = ENDED;
      schedule.timer.stop();
      schedule.on.call(active ? "interrupt" : "cancel", node, node.__data__, schedule.index, schedule.group);
      delete schedules[i];
    }

    if (empty) delete node.__transition;
  }

  function selection_interrupt(name) {
    return this.each(function() {
      interrupt(this, name);
    });
  }

  function define(constructor, factory, prototype) {
    constructor.prototype = factory.prototype = prototype;
    prototype.constructor = constructor;
  }

  function extend(parent, definition) {
    var prototype = Object.create(parent.prototype);
    for (var key in definition) prototype[key] = definition[key];
    return prototype;
  }

  function Color() {}

  var darker = 0.7;
  var brighter = 1 / darker;

  var reI = "\\s*([+-]?\\d+)\\s*",
      reN = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)\\s*",
      reP = "\\s*([+-]?\\d*\\.?\\d+(?:[eE][+-]?\\d+)?)%\\s*",
      reHex = /^#([0-9a-f]{3,8})$/,
      reRgbInteger = new RegExp("^rgb\\(" + [reI, reI, reI] + "\\)$"),
      reRgbPercent = new RegExp("^rgb\\(" + [reP, reP, reP] + "\\)$"),
      reRgbaInteger = new RegExp("^rgba\\(" + [reI, reI, reI, reN] + "\\)$"),
      reRgbaPercent = new RegExp("^rgba\\(" + [reP, reP, reP, reN] + "\\)$"),
      reHslPercent = new RegExp("^hsl\\(" + [reN, reP, reP] + "\\)$"),
      reHslaPercent = new RegExp("^hsla\\(" + [reN, reP, reP, reN] + "\\)$");

  var named = {
    aliceblue: 0xf0f8ff,
    antiquewhite: 0xfaebd7,
    aqua: 0x00ffff,
    aquamarine: 0x7fffd4,
    azure: 0xf0ffff,
    beige: 0xf5f5dc,
    bisque: 0xffe4c4,
    black: 0x000000,
    blanchedalmond: 0xffebcd,
    blue: 0x0000ff,
    blueviolet: 0x8a2be2,
    brown: 0xa52a2a,
    burlywood: 0xdeb887,
    cadetblue: 0x5f9ea0,
    chartreuse: 0x7fff00,
    chocolate: 0xd2691e,
    coral: 0xff7f50,
    cornflowerblue: 0x6495ed,
    cornsilk: 0xfff8dc,
    crimson: 0xdc143c,
    cyan: 0x00ffff,
    darkblue: 0x00008b,
    darkcyan: 0x008b8b,
    darkgoldenrod: 0xb8860b,
    darkgray: 0xa9a9a9,
    darkgreen: 0x006400,
    darkgrey: 0xa9a9a9,
    darkkhaki: 0xbdb76b,
    darkmagenta: 0x8b008b,
    darkolivegreen: 0x556b2f,
    darkorange: 0xff8c00,
    darkorchid: 0x9932cc,
    darkred: 0x8b0000,
    darksalmon: 0xe9967a,
    darkseagreen: 0x8fbc8f,
    darkslateblue: 0x483d8b,
    darkslategray: 0x2f4f4f,
    darkslategrey: 0x2f4f4f,
    darkturquoise: 0x00ced1,
    darkviolet: 0x9400d3,
    deeppink: 0xff1493,
    deepskyblue: 0x00bfff,
    dimgray: 0x696969,
    dimgrey: 0x696969,
    dodgerblue: 0x1e90ff,
    firebrick: 0xb22222,
    floralwhite: 0xfffaf0,
    forestgreen: 0x228b22,
    fuchsia: 0xff00ff,
    gainsboro: 0xdcdcdc,
    ghostwhite: 0xf8f8ff,
    gold: 0xffd700,
    goldenrod: 0xdaa520,
    gray: 0x808080,
    green: 0x008000,
    greenyellow: 0xadff2f,
    grey: 0x808080,
    honeydew: 0xf0fff0,
    hotpink: 0xff69b4,
    indianred: 0xcd5c5c,
    indigo: 0x4b0082,
    ivory: 0xfffff0,
    khaki: 0xf0e68c,
    lavender: 0xe6e6fa,
    lavenderblush: 0xfff0f5,
    lawngreen: 0x7cfc00,
    lemonchiffon: 0xfffacd,
    lightblue: 0xadd8e6,
    lightcoral: 0xf08080,
    lightcyan: 0xe0ffff,
    lightgoldenrodyellow: 0xfafad2,
    lightgray: 0xd3d3d3,
    lightgreen: 0x90ee90,
    lightgrey: 0xd3d3d3,
    lightpink: 0xffb6c1,
    lightsalmon: 0xffa07a,
    lightseagreen: 0x20b2aa,
    lightskyblue: 0x87cefa,
    lightslategray: 0x778899,
    lightslategrey: 0x778899,
    lightsteelblue: 0xb0c4de,
    lightyellow: 0xffffe0,
    lime: 0x00ff00,
    limegreen: 0x32cd32,
    linen: 0xfaf0e6,
    magenta: 0xff00ff,
    maroon: 0x800000,
    mediumaquamarine: 0x66cdaa,
    mediumblue: 0x0000cd,
    mediumorchid: 0xba55d3,
    mediumpurple: 0x9370db,
    mediumseagreen: 0x3cb371,
    mediumslateblue: 0x7b68ee,
    mediumspringgreen: 0x00fa9a,
    mediumturquoise: 0x48d1cc,
    mediumvioletred: 0xc71585,
    midnightblue: 0x191970,
    mintcream: 0xf5fffa,
    mistyrose: 0xffe4e1,
    moccasin: 0xffe4b5,
    navajowhite: 0xffdead,
    navy: 0x000080,
    oldlace: 0xfdf5e6,
    olive: 0x808000,
    olivedrab: 0x6b8e23,
    orange: 0xffa500,
    orangered: 0xff4500,
    orchid: 0xda70d6,
    palegoldenrod: 0xeee8aa,
    palegreen: 0x98fb98,
    paleturquoise: 0xafeeee,
    palevioletred: 0xdb7093,
    papayawhip: 0xffefd5,
    peachpuff: 0xffdab9,
    peru: 0xcd853f,
    pink: 0xffc0cb,
    plum: 0xdda0dd,
    powderblue: 0xb0e0e6,
    purple: 0x800080,
    rebeccapurple: 0x663399,
    red: 0xff0000,
    rosybrown: 0xbc8f8f,
    royalblue: 0x4169e1,
    saddlebrown: 0x8b4513,
    salmon: 0xfa8072,
    sandybrown: 0xf4a460,
    seagreen: 0x2e8b57,
    seashell: 0xfff5ee,
    sienna: 0xa0522d,
    silver: 0xc0c0c0,
    skyblue: 0x87ceeb,
    slateblue: 0x6a5acd,
    slategray: 0x708090,
    slategrey: 0x708090,
    snow: 0xfffafa,
    springgreen: 0x00ff7f,
    steelblue: 0x4682b4,
    tan: 0xd2b48c,
    teal: 0x008080,
    thistle: 0xd8bfd8,
    tomato: 0xff6347,
    turquoise: 0x40e0d0,
    violet: 0xee82ee,
    wheat: 0xf5deb3,
    white: 0xffffff,
    whitesmoke: 0xf5f5f5,
    yellow: 0xffff00,
    yellowgreen: 0x9acd32
  };

  define(Color, color, {
    copy: function(channels) {
      return Object.assign(new this.constructor, this, channels);
    },
    displayable: function() {
      return this.rgb().displayable();
    },
    hex: color_formatHex, // Deprecated! Use color.formatHex.
    formatHex: color_formatHex,
    formatHsl: color_formatHsl,
    formatRgb: color_formatRgb,
    toString: color_formatRgb
  });

  function color_formatHex() {
    return this.rgb().formatHex();
  }

  function color_formatHsl() {
    return hslConvert(this).formatHsl();
  }

  function color_formatRgb() {
    return this.rgb().formatRgb();
  }

  function color(format) {
    var m, l;
    format = (format + "").trim().toLowerCase();
    return (m = reHex.exec(format)) ? (l = m[1].length, m = parseInt(m[1], 16), l === 6 ? rgbn(m) // #ff0000
        : l === 3 ? new Rgb((m >> 8 & 0xf) | (m >> 4 & 0xf0), (m >> 4 & 0xf) | (m & 0xf0), ((m & 0xf) << 4) | (m & 0xf), 1) // #f00
        : l === 8 ? new Rgb(m >> 24 & 0xff, m >> 16 & 0xff, m >> 8 & 0xff, (m & 0xff) / 0xff) // #ff000000
        : l === 4 ? new Rgb((m >> 12 & 0xf) | (m >> 8 & 0xf0), (m >> 8 & 0xf) | (m >> 4 & 0xf0), (m >> 4 & 0xf) | (m & 0xf0), (((m & 0xf) << 4) | (m & 0xf)) / 0xff) // #f000
        : null) // invalid hex
        : (m = reRgbInteger.exec(format)) ? new Rgb(m[1], m[2], m[3], 1) // rgb(255, 0, 0)
        : (m = reRgbPercent.exec(format)) ? new Rgb(m[1] * 255 / 100, m[2] * 255 / 100, m[3] * 255 / 100, 1) // rgb(100%, 0%, 0%)
        : (m = reRgbaInteger.exec(format)) ? rgba(m[1], m[2], m[3], m[4]) // rgba(255, 0, 0, 1)
        : (m = reRgbaPercent.exec(format)) ? rgba(m[1] * 255 / 100, m[2] * 255 / 100, m[3] * 255 / 100, m[4]) // rgb(100%, 0%, 0%, 1)
        : (m = reHslPercent.exec(format)) ? hsla(m[1], m[2] / 100, m[3] / 100, 1) // hsl(120, 50%, 50%)
        : (m = reHslaPercent.exec(format)) ? hsla(m[1], m[2] / 100, m[3] / 100, m[4]) // hsla(120, 50%, 50%, 1)
        : named.hasOwnProperty(format) ? rgbn(named[format]) // eslint-disable-line no-prototype-builtins
        : format === "transparent" ? new Rgb(NaN, NaN, NaN, 0)
        : null;
  }

  function rgbn(n) {
    return new Rgb(n >> 16 & 0xff, n >> 8 & 0xff, n & 0xff, 1);
  }

  function rgba(r, g, b, a) {
    if (a <= 0) r = g = b = NaN;
    return new Rgb(r, g, b, a);
  }

  function rgbConvert(o) {
    if (!(o instanceof Color)) o = color(o);
    if (!o) return new Rgb;
    o = o.rgb();
    return new Rgb(o.r, o.g, o.b, o.opacity);
  }

  function rgb(r, g, b, opacity) {
    return arguments.length === 1 ? rgbConvert(r) : new Rgb(r, g, b, opacity == null ? 1 : opacity);
  }

  function Rgb(r, g, b, opacity) {
    this.r = +r;
    this.g = +g;
    this.b = +b;
    this.opacity = +opacity;
  }

  define(Rgb, rgb, extend(Color, {
    brighter: function(k) {
      k = k == null ? brighter : Math.pow(brighter, k);
      return new Rgb(this.r * k, this.g * k, this.b * k, this.opacity);
    },
    darker: function(k) {
      k = k == null ? darker : Math.pow(darker, k);
      return new Rgb(this.r * k, this.g * k, this.b * k, this.opacity);
    },
    rgb: function() {
      return this;
    },
    displayable: function() {
      return (-0.5 <= this.r && this.r < 255.5)
          && (-0.5 <= this.g && this.g < 255.5)
          && (-0.5 <= this.b && this.b < 255.5)
          && (0 <= this.opacity && this.opacity <= 1);
    },
    hex: rgb_formatHex, // Deprecated! Use color.formatHex.
    formatHex: rgb_formatHex,
    formatRgb: rgb_formatRgb,
    toString: rgb_formatRgb
  }));

  function rgb_formatHex() {
    return "#" + hex(this.r) + hex(this.g) + hex(this.b);
  }

  function rgb_formatRgb() {
    var a = this.opacity; a = isNaN(a) ? 1 : Math.max(0, Math.min(1, a));
    return (a === 1 ? "rgb(" : "rgba(")
        + Math.max(0, Math.min(255, Math.round(this.r) || 0)) + ", "
        + Math.max(0, Math.min(255, Math.round(this.g) || 0)) + ", "
        + Math.max(0, Math.min(255, Math.round(this.b) || 0))
        + (a === 1 ? ")" : ", " + a + ")");
  }

  function hex(value) {
    value = Math.max(0, Math.min(255, Math.round(value) || 0));
    return (value < 16 ? "0" : "") + value.toString(16);
  }

  function hsla(h, s, l, a) {
    if (a <= 0) h = s = l = NaN;
    else if (l <= 0 || l >= 1) h = s = NaN;
    else if (s <= 0) h = NaN;
    return new Hsl(h, s, l, a);
  }

  function hslConvert(o) {
    if (o instanceof Hsl) return new Hsl(o.h, o.s, o.l, o.opacity);
    if (!(o instanceof Color)) o = color(o);
    if (!o) return new Hsl;
    if (o instanceof Hsl) return o;
    o = o.rgb();
    var r = o.r / 255,
        g = o.g / 255,
        b = o.b / 255,
        min = Math.min(r, g, b),
        max = Math.max(r, g, b),
        h = NaN,
        s = max - min,
        l = (max + min) / 2;
    if (s) {
      if (r === max) h = (g - b) / s + (g < b) * 6;
      else if (g === max) h = (b - r) / s + 2;
      else h = (r - g) / s + 4;
      s /= l < 0.5 ? max + min : 2 - max - min;
      h *= 60;
    } else {
      s = l > 0 && l < 1 ? 0 : h;
    }
    return new Hsl(h, s, l, o.opacity);
  }

  function hsl(h, s, l, opacity) {
    return arguments.length === 1 ? hslConvert(h) : new Hsl(h, s, l, opacity == null ? 1 : opacity);
  }

  function Hsl(h, s, l, opacity) {
    this.h = +h;
    this.s = +s;
    this.l = +l;
    this.opacity = +opacity;
  }

  define(Hsl, hsl, extend(Color, {
    brighter: function(k) {
      k = k == null ? brighter : Math.pow(brighter, k);
      return new Hsl(this.h, this.s, this.l * k, this.opacity);
    },
    darker: function(k) {
      k = k == null ? darker : Math.pow(darker, k);
      return new Hsl(this.h, this.s, this.l * k, this.opacity);
    },
    rgb: function() {
      var h = this.h % 360 + (this.h < 0) * 360,
          s = isNaN(h) || isNaN(this.s) ? 0 : this.s,
          l = this.l,
          m2 = l + (l < 0.5 ? l : 1 - l) * s,
          m1 = 2 * l - m2;
      return new Rgb(
        hsl2rgb(h >= 240 ? h - 240 : h + 120, m1, m2),
        hsl2rgb(h, m1, m2),
        hsl2rgb(h < 120 ? h + 240 : h - 120, m1, m2),
        this.opacity
      );
    },
    displayable: function() {
      return (0 <= this.s && this.s <= 1 || isNaN(this.s))
          && (0 <= this.l && this.l <= 1)
          && (0 <= this.opacity && this.opacity <= 1);
    },
    formatHsl: function() {
      var a = this.opacity; a = isNaN(a) ? 1 : Math.max(0, Math.min(1, a));
      return (a === 1 ? "hsl(" : "hsla(")
          + (this.h || 0) + ", "
          + (this.s || 0) * 100 + "%, "
          + (this.l || 0) * 100 + "%"
          + (a === 1 ? ")" : ", " + a + ")");
    }
  }));

  /* From FvD 13.37, CSS Color Module Level 3 */
  function hsl2rgb(h, m1, m2) {
    return (h < 60 ? m1 + (m2 - m1) * h / 60
        : h < 180 ? m2
        : h < 240 ? m1 + (m2 - m1) * (240 - h) / 60
        : m1) * 255;
  }

  var deg2rad = Math.PI / 180;
  var rad2deg = 180 / Math.PI;

  var A = -0.14861,
      B = +1.78277,
      C = -0.29227,
      D = -0.90649,
      E = +1.97294,
      ED = E * D,
      EB = E * B,
      BC_DA = B * C - D * A;

  function cubehelixConvert(o) {
    if (o instanceof Cubehelix) return new Cubehelix(o.h, o.s, o.l, o.opacity);
    if (!(o instanceof Rgb)) o = rgbConvert(o);
    var r = o.r / 255,
        g = o.g / 255,
        b = o.b / 255,
        l = (BC_DA * b + ED * r - EB * g) / (BC_DA + ED - EB),
        bl = b - l,
        k = (E * (g - l) - C * bl) / D,
        s = Math.sqrt(k * k + bl * bl) / (E * l * (1 - l)), // NaN if l=0 or l=1
        h = s ? Math.atan2(k, bl) * rad2deg - 120 : NaN;
    return new Cubehelix(h < 0 ? h + 360 : h, s, l, o.opacity);
  }

  function cubehelix(h, s, l, opacity) {
    return arguments.length === 1 ? cubehelixConvert(h) : new Cubehelix(h, s, l, opacity == null ? 1 : opacity);
  }

  function Cubehelix(h, s, l, opacity) {
    this.h = +h;
    this.s = +s;
    this.l = +l;
    this.opacity = +opacity;
  }

  define(Cubehelix, cubehelix, extend(Color, {
    brighter: function(k) {
      k = k == null ? brighter : Math.pow(brighter, k);
      return new Cubehelix(this.h, this.s, this.l * k, this.opacity);
    },
    darker: function(k) {
      k = k == null ? darker : Math.pow(darker, k);
      return new Cubehelix(this.h, this.s, this.l * k, this.opacity);
    },
    rgb: function() {
      var h = isNaN(this.h) ? 0 : (this.h + 120) * deg2rad,
          l = +this.l,
          a = isNaN(this.s) ? 0 : this.s * l * (1 - l),
          cosh = Math.cos(h),
          sinh = Math.sin(h);
      return new Rgb(
        255 * (l + a * (A * cosh + B * sinh)),
        255 * (l + a * (C * cosh + D * sinh)),
        255 * (l + a * (E * cosh)),
        this.opacity
      );
    }
  }));

  function constant$1(x) {
    return function() {
      return x;
    };
  }

  function linear(a, d) {
    return function(t) {
      return a + t * d;
    };
  }

  function exponential(a, b, y) {
    return a = Math.pow(a, y), b = Math.pow(b, y) - a, y = 1 / y, function(t) {
      return Math.pow(a + t * b, y);
    };
  }

  function hue(a, b) {
    var d = b - a;
    return d ? linear(a, d > 180 || d < -180 ? d - 360 * Math.round(d / 360) : d) : constant$1(isNaN(a) ? b : a);
  }

  function gamma(y) {
    return (y = +y) === 1 ? nogamma : function(a, b) {
      return b - a ? exponential(a, b, y) : constant$1(isNaN(a) ? b : a);
    };
  }

  function nogamma(a, b) {
    var d = b - a;
    return d ? linear(a, d) : constant$1(isNaN(a) ? b : a);
  }

  var interpolateRgb = (function rgbGamma(y) {
    var color = gamma(y);

    function rgb$1(start, end) {
      var r = color((start = rgb(start)).r, (end = rgb(end)).r),
          g = color(start.g, end.g),
          b = color(start.b, end.b),
          opacity = nogamma(start.opacity, end.opacity);
      return function(t) {
        start.r = r(t);
        start.g = g(t);
        start.b = b(t);
        start.opacity = opacity(t);
        return start + "";
      };
    }

    rgb$1.gamma = rgbGamma;

    return rgb$1;
  })(1);

  function interpolateNumber(a, b) {
    return a = +a, b -= a, function(t) {
      return a + b * t;
    };
  }

  var reA = /[-+]?(?:\d+\.?\d*|\.?\d+)(?:[eE][-+]?\d+)?/g,
      reB = new RegExp(reA.source, "g");

  function zero(b) {
    return function() {
      return b;
    };
  }

  function one(b) {
    return function(t) {
      return b(t) + "";
    };
  }

  function interpolateString(a, b) {
    var bi = reA.lastIndex = reB.lastIndex = 0, // scan index for next number in b
        am, // current match in a
        bm, // current match in b
        bs, // string preceding current number in b, if any
        i = -1, // index in s
        s = [], // string constants and placeholders
        q = []; // number interpolators

    // Coerce inputs to strings.
    a = a + "", b = b + "";

    // Interpolate pairs of numbers in a & b.
    while ((am = reA.exec(a))
        && (bm = reB.exec(b))) {
      if ((bs = bm.index) > bi) { // a string precedes the next number in b
        bs = b.slice(bi, bs);
        if (s[i]) s[i] += bs; // coalesce with previous string
        else s[++i] = bs;
      }
      if ((am = am[0]) === (bm = bm[0])) { // numbers in a & b match
        if (s[i]) s[i] += bm; // coalesce with previous string
        else s[++i] = bm;
      } else { // interpolate non-matching numbers
        s[++i] = null;
        q.push({i: i, x: interpolateNumber(am, bm)});
      }
      bi = reB.lastIndex;
    }

    // Add remains of b.
    if (bi < b.length) {
      bs = b.slice(bi);
      if (s[i]) s[i] += bs; // coalesce with previous string
      else s[++i] = bs;
    }

    // Special optimization for only a single match.
    // Otherwise, interpolate each of the numbers and rejoin the string.
    return s.length < 2 ? (q[0]
        ? one(q[0].x)
        : zero(b))
        : (b = q.length, function(t) {
            for (var i = 0, o; i < b; ++i) s[(o = q[i]).i] = o.x(t);
            return s.join("");
          });
  }

  var degrees = 180 / Math.PI;

  var identity = {
    translateX: 0,
    translateY: 0,
    rotate: 0,
    skewX: 0,
    scaleX: 1,
    scaleY: 1
  };

  function decompose(a, b, c, d, e, f) {
    var scaleX, scaleY, skewX;
    if (scaleX = Math.sqrt(a * a + b * b)) a /= scaleX, b /= scaleX;
    if (skewX = a * c + b * d) c -= a * skewX, d -= b * skewX;
    if (scaleY = Math.sqrt(c * c + d * d)) c /= scaleY, d /= scaleY, skewX /= scaleY;
    if (a * d < b * c) a = -a, b = -b, skewX = -skewX, scaleX = -scaleX;
    return {
      translateX: e,
      translateY: f,
      rotate: Math.atan2(b, a) * degrees,
      skewX: Math.atan(skewX) * degrees,
      scaleX: scaleX,
      scaleY: scaleY
    };
  }

  var cssNode,
      cssRoot,
      cssView,
      svgNode;

  function parseCss(value) {
    if (value === "none") return identity;
    if (!cssNode) cssNode = document.createElement("DIV"), cssRoot = document.documentElement, cssView = document.defaultView;
    cssNode.style.transform = value;
    value = cssView.getComputedStyle(cssRoot.appendChild(cssNode), null).getPropertyValue("transform");
    cssRoot.removeChild(cssNode);
    value = value.slice(7, -1).split(",");
    return decompose(+value[0], +value[1], +value[2], +value[3], +value[4], +value[5]);
  }

  function parseSvg(value) {
    if (value == null) return identity;
    if (!svgNode) svgNode = document.createElementNS("http://www.w3.org/2000/svg", "g");
    svgNode.setAttribute("transform", value);
    if (!(value = svgNode.transform.baseVal.consolidate())) return identity;
    value = value.matrix;
    return decompose(value.a, value.b, value.c, value.d, value.e, value.f);
  }

  function interpolateTransform(parse, pxComma, pxParen, degParen) {

    function pop(s) {
      return s.length ? s.pop() + " " : "";
    }

    function translate(xa, ya, xb, yb, s, q) {
      if (xa !== xb || ya !== yb) {
        var i = s.push("translate(", null, pxComma, null, pxParen);
        q.push({i: i - 4, x: interpolateNumber(xa, xb)}, {i: i - 2, x: interpolateNumber(ya, yb)});
      } else if (xb || yb) {
        s.push("translate(" + xb + pxComma + yb + pxParen);
      }
    }

    function rotate(a, b, s, q) {
      if (a !== b) {
        if (a - b > 180) b += 360; else if (b - a > 180) a += 360; // shortest path
        q.push({i: s.push(pop(s) + "rotate(", null, degParen) - 2, x: interpolateNumber(a, b)});
      } else if (b) {
        s.push(pop(s) + "rotate(" + b + degParen);
      }
    }

    function skewX(a, b, s, q) {
      if (a !== b) {
        q.push({i: s.push(pop(s) + "skewX(", null, degParen) - 2, x: interpolateNumber(a, b)});
      } else if (b) {
        s.push(pop(s) + "skewX(" + b + degParen);
      }
    }

    function scale(xa, ya, xb, yb, s, q) {
      if (xa !== xb || ya !== yb) {
        var i = s.push(pop(s) + "scale(", null, ",", null, ")");
        q.push({i: i - 4, x: interpolateNumber(xa, xb)}, {i: i - 2, x: interpolateNumber(ya, yb)});
      } else if (xb !== 1 || yb !== 1) {
        s.push(pop(s) + "scale(" + xb + "," + yb + ")");
      }
    }

    return function(a, b) {
      var s = [], // string constants and placeholders
          q = []; // number interpolators
      a = parse(a), b = parse(b);
      translate(a.translateX, a.translateY, b.translateX, b.translateY, s, q);
      rotate(a.rotate, b.rotate, s, q);
      skewX(a.skewX, b.skewX, s, q);
      scale(a.scaleX, a.scaleY, b.scaleX, b.scaleY, s, q);
      a = b = null; // gc
      return function(t) {
        var i = -1, n = q.length, o;
        while (++i < n) s[(o = q[i]).i] = o.x(t);
        return s.join("");
      };
    };
  }

  var interpolateTransformCss = interpolateTransform(parseCss, "px, ", "px)", "deg)");
  var interpolateTransformSvg = interpolateTransform(parseSvg, ", ", ")", ")");

  function cubehelix$1(hue) {
    return (function cubehelixGamma(y) {
      y = +y;

      function cubehelix$1(start, end) {
        var h = hue((start = cubehelix(start)).h, (end = cubehelix(end)).h),
            s = nogamma(start.s, end.s),
            l = nogamma(start.l, end.l),
            opacity = nogamma(start.opacity, end.opacity);
        return function(t) {
          start.h = h(t);
          start.s = s(t);
          start.l = l(Math.pow(t, y));
          start.opacity = opacity(t);
          return start + "";
        };
      }

      cubehelix$1.gamma = cubehelixGamma;

      return cubehelix$1;
    })(1);
  }

  cubehelix$1(hue);
  var cubehelixLong = cubehelix$1(nogamma);

  function tweenRemove(id, name) {
    var tween0, tween1;
    return function() {
      var schedule = set$1(this, id),
          tween = schedule.tween;

      // If this node shared tween with the previous node,
      // just assign the updated shared tween and we’re done!
      // Otherwise, copy-on-write.
      if (tween !== tween0) {
        tween1 = tween0 = tween;
        for (var i = 0, n = tween1.length; i < n; ++i) {
          if (tween1[i].name === name) {
            tween1 = tween1.slice();
            tween1.splice(i, 1);
            break;
          }
        }
      }

      schedule.tween = tween1;
    };
  }

  function tweenFunction(id, name, value) {
    var tween0, tween1;
    if (typeof value !== "function") throw new Error;
    return function() {
      var schedule = set$1(this, id),
          tween = schedule.tween;

      // If this node shared tween with the previous node,
      // just assign the updated shared tween and we’re done!
      // Otherwise, copy-on-write.
      if (tween !== tween0) {
        tween1 = (tween0 = tween).slice();
        for (var t = {name: name, value: value}, i = 0, n = tween1.length; i < n; ++i) {
          if (tween1[i].name === name) {
            tween1[i] = t;
            break;
          }
        }
        if (i === n) tween1.push(t);
      }

      schedule.tween = tween1;
    };
  }

  function transition_tween(name, value) {
    var id = this._id;

    name += "";

    if (arguments.length < 2) {
      var tween = get$1(this.node(), id).tween;
      for (var i = 0, n = tween.length, t; i < n; ++i) {
        if ((t = tween[i]).name === name) {
          return t.value;
        }
      }
      return null;
    }

    return this.each((value == null ? tweenRemove : tweenFunction)(id, name, value));
  }

  function tweenValue(transition, name, value) {
    var id = transition._id;

    transition.each(function() {
      var schedule = set$1(this, id);
      (schedule.value || (schedule.value = {}))[name] = value.apply(this, arguments);
    });

    return function(node) {
      return get$1(node, id).value[name];
    };
  }

  function interpolate(a, b) {
    var c;
    return (typeof b === "number" ? interpolateNumber
        : b instanceof color ? interpolateRgb
        : (c = color(b)) ? (b = c, interpolateRgb)
        : interpolateString)(a, b);
  }

  function attrRemove$1(name) {
    return function() {
      this.removeAttribute(name);
    };
  }

  function attrRemoveNS$1(fullname) {
    return function() {
      this.removeAttributeNS(fullname.space, fullname.local);
    };
  }

  function attrConstant$1(name, interpolate, value1) {
    var string00,
        string1 = value1 + "",
        interpolate0;
    return function() {
      var string0 = this.getAttribute(name);
      return string0 === string1 ? null
          : string0 === string00 ? interpolate0
          : interpolate0 = interpolate(string00 = string0, value1);
    };
  }

  function attrConstantNS$1(fullname, interpolate, value1) {
    var string00,
        string1 = value1 + "",
        interpolate0;
    return function() {
      var string0 = this.getAttributeNS(fullname.space, fullname.local);
      return string0 === string1 ? null
          : string0 === string00 ? interpolate0
          : interpolate0 = interpolate(string00 = string0, value1);
    };
  }

  function attrFunction$1(name, interpolate, value) {
    var string00,
        string10,
        interpolate0;
    return function() {
      var string0, value1 = value(this), string1;
      if (value1 == null) return void this.removeAttribute(name);
      string0 = this.getAttribute(name);
      string1 = value1 + "";
      return string0 === string1 ? null
          : string0 === string00 && string1 === string10 ? interpolate0
          : (string10 = string1, interpolate0 = interpolate(string00 = string0, value1));
    };
  }

  function attrFunctionNS$1(fullname, interpolate, value) {
    var string00,
        string10,
        interpolate0;
    return function() {
      var string0, value1 = value(this), string1;
      if (value1 == null) return void this.removeAttributeNS(fullname.space, fullname.local);
      string0 = this.getAttributeNS(fullname.space, fullname.local);
      string1 = value1 + "";
      return string0 === string1 ? null
          : string0 === string00 && string1 === string10 ? interpolate0
          : (string10 = string1, interpolate0 = interpolate(string00 = string0, value1));
    };
  }

  function transition_attr(name, value) {
    var fullname = namespace(name), i = fullname === "transform" ? interpolateTransformSvg : interpolate;
    return this.attrTween(name, typeof value === "function"
        ? (fullname.local ? attrFunctionNS$1 : attrFunction$1)(fullname, i, tweenValue(this, "attr." + name, value))
        : value == null ? (fullname.local ? attrRemoveNS$1 : attrRemove$1)(fullname)
        : (fullname.local ? attrConstantNS$1 : attrConstant$1)(fullname, i, value));
  }

  function attrInterpolate(name, i) {
    return function(t) {
      this.setAttribute(name, i(t));
    };
  }

  function attrInterpolateNS(fullname, i) {
    return function(t) {
      this.setAttributeNS(fullname.space, fullname.local, i(t));
    };
  }

  function attrTweenNS(fullname, value) {
    var t0, i0;
    function tween() {
      var i = value.apply(this, arguments);
      if (i !== i0) t0 = (i0 = i) && attrInterpolateNS(fullname, i);
      return t0;
    }
    tween._value = value;
    return tween;
  }

  function attrTween(name, value) {
    var t0, i0;
    function tween() {
      var i = value.apply(this, arguments);
      if (i !== i0) t0 = (i0 = i) && attrInterpolate(name, i);
      return t0;
    }
    tween._value = value;
    return tween;
  }

  function transition_attrTween(name, value) {
    var key = "attr." + name;
    if (arguments.length < 2) return (key = this.tween(key)) && key._value;
    if (value == null) return this.tween(key, null);
    if (typeof value !== "function") throw new Error;
    var fullname = namespace(name);
    return this.tween(key, (fullname.local ? attrTweenNS : attrTween)(fullname, value));
  }

  function delayFunction(id, value) {
    return function() {
      init(this, id).delay = +value.apply(this, arguments);
    };
  }

  function delayConstant(id, value) {
    return value = +value, function() {
      init(this, id).delay = value;
    };
  }

  function transition_delay(value) {
    var id = this._id;

    return arguments.length
        ? this.each((typeof value === "function"
            ? delayFunction
            : delayConstant)(id, value))
        : get$1(this.node(), id).delay;
  }

  function durationFunction(id, value) {
    return function() {
      set$1(this, id).duration = +value.apply(this, arguments);
    };
  }

  function durationConstant(id, value) {
    return value = +value, function() {
      set$1(this, id).duration = value;
    };
  }

  function transition_duration(value) {
    var id = this._id;

    return arguments.length
        ? this.each((typeof value === "function"
            ? durationFunction
            : durationConstant)(id, value))
        : get$1(this.node(), id).duration;
  }

  function easeConstant(id, value) {
    if (typeof value !== "function") throw new Error;
    return function() {
      set$1(this, id).ease = value;
    };
  }

  function transition_ease(value) {
    var id = this._id;

    return arguments.length
        ? this.each(easeConstant(id, value))
        : get$1(this.node(), id).ease;
  }

  function transition_filter(match) {
    if (typeof match !== "function") match = matcher(match);

    for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, subgroup = subgroups[j] = [], node, i = 0; i < n; ++i) {
        if ((node = group[i]) && match.call(node, node.__data__, i, group)) {
          subgroup.push(node);
        }
      }
    }

    return new Transition(subgroups, this._parents, this._name, this._id);
  }

  function transition_merge(transition) {
    if (transition._id !== this._id) throw new Error;

    for (var groups0 = this._groups, groups1 = transition._groups, m0 = groups0.length, m1 = groups1.length, m = Math.min(m0, m1), merges = new Array(m0), j = 0; j < m; ++j) {
      for (var group0 = groups0[j], group1 = groups1[j], n = group0.length, merge = merges[j] = new Array(n), node, i = 0; i < n; ++i) {
        if (node = group0[i] || group1[i]) {
          merge[i] = node;
        }
      }
    }

    for (; j < m0; ++j) {
      merges[j] = groups0[j];
    }

    return new Transition(merges, this._parents, this._name, this._id);
  }

  function start(name) {
    return (name + "").trim().split(/^|\s+/).every(function(t) {
      var i = t.indexOf(".");
      if (i >= 0) t = t.slice(0, i);
      return !t || t === "start";
    });
  }

  function onFunction(id, name, listener) {
    var on0, on1, sit = start(name) ? init : set$1;
    return function() {
      var schedule = sit(this, id),
          on = schedule.on;

      // If this node shared a dispatch with the previous node,
      // just assign the updated shared dispatch and we’re done!
      // Otherwise, copy-on-write.
      if (on !== on0) (on1 = (on0 = on).copy()).on(name, listener);

      schedule.on = on1;
    };
  }

  function transition_on(name, listener) {
    var id = this._id;

    return arguments.length < 2
        ? get$1(this.node(), id).on.on(name)
        : this.each(onFunction(id, name, listener));
  }

  function removeFunction(id) {
    return function() {
      var parent = this.parentNode;
      for (var i in this.__transition) if (+i !== id) return;
      if (parent) parent.removeChild(this);
    };
  }

  function transition_remove() {
    return this.on("end.remove", removeFunction(this._id));
  }

  function transition_select(select) {
    var name = this._name,
        id = this._id;

    if (typeof select !== "function") select = selector(select);

    for (var groups = this._groups, m = groups.length, subgroups = new Array(m), j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, subgroup = subgroups[j] = new Array(n), node, subnode, i = 0; i < n; ++i) {
        if ((node = group[i]) && (subnode = select.call(node, node.__data__, i, group))) {
          if ("__data__" in node) subnode.__data__ = node.__data__;
          subgroup[i] = subnode;
          schedule(subgroup[i], name, id, i, subgroup, get$1(node, id));
        }
      }
    }

    return new Transition(subgroups, this._parents, name, id);
  }

  function transition_selectAll(select) {
    var name = this._name,
        id = this._id;

    if (typeof select !== "function") select = selectorAll(select);

    for (var groups = this._groups, m = groups.length, subgroups = [], parents = [], j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
        if (node = group[i]) {
          for (var children = select.call(node, node.__data__, i, group), child, inherit = get$1(node, id), k = 0, l = children.length; k < l; ++k) {
            if (child = children[k]) {
              schedule(child, name, id, k, children, inherit);
            }
          }
          subgroups.push(children);
          parents.push(node);
        }
      }
    }

    return new Transition(subgroups, parents, name, id);
  }

  var Selection$1 = selection.prototype.constructor;

  function transition_selection() {
    return new Selection$1(this._groups, this._parents);
  }

  function styleNull(name, interpolate) {
    var string00,
        string10,
        interpolate0;
    return function() {
      var string0 = styleValue(this, name),
          string1 = (this.style.removeProperty(name), styleValue(this, name));
      return string0 === string1 ? null
          : string0 === string00 && string1 === string10 ? interpolate0
          : interpolate0 = interpolate(string00 = string0, string10 = string1);
    };
  }

  function styleRemove$1(name) {
    return function() {
      this.style.removeProperty(name);
    };
  }

  function styleConstant$1(name, interpolate, value1) {
    var string00,
        string1 = value1 + "",
        interpolate0;
    return function() {
      var string0 = styleValue(this, name);
      return string0 === string1 ? null
          : string0 === string00 ? interpolate0
          : interpolate0 = interpolate(string00 = string0, value1);
    };
  }

  function styleFunction$1(name, interpolate, value) {
    var string00,
        string10,
        interpolate0;
    return function() {
      var string0 = styleValue(this, name),
          value1 = value(this),
          string1 = value1 + "";
      if (value1 == null) string1 = value1 = (this.style.removeProperty(name), styleValue(this, name));
      return string0 === string1 ? null
          : string0 === string00 && string1 === string10 ? interpolate0
          : (string10 = string1, interpolate0 = interpolate(string00 = string0, value1));
    };
  }

  function styleMaybeRemove(id, name) {
    var on0, on1, listener0, key = "style." + name, event = "end." + key, remove;
    return function() {
      var schedule = set$1(this, id),
          on = schedule.on,
          listener = schedule.value[key] == null ? remove || (remove = styleRemove$1(name)) : undefined;

      // If this node shared a dispatch with the previous node,
      // just assign the updated shared dispatch and we’re done!
      // Otherwise, copy-on-write.
      if (on !== on0 || listener0 !== listener) (on1 = (on0 = on).copy()).on(event, listener0 = listener);

      schedule.on = on1;
    };
  }

  function transition_style(name, value, priority) {
    var i = (name += "") === "transform" ? interpolateTransformCss : interpolate;
    return value == null ? this
        .styleTween(name, styleNull(name, i))
        .on("end.style." + name, styleRemove$1(name))
      : typeof value === "function" ? this
        .styleTween(name, styleFunction$1(name, i, tweenValue(this, "style." + name, value)))
        .each(styleMaybeRemove(this._id, name))
      : this
        .styleTween(name, styleConstant$1(name, i, value), priority)
        .on("end.style." + name, null);
  }

  function styleInterpolate(name, i, priority) {
    return function(t) {
      this.style.setProperty(name, i(t), priority);
    };
  }

  function styleTween(name, value, priority) {
    var t, i0;
    function tween() {
      var i = value.apply(this, arguments);
      if (i !== i0) t = (i0 = i) && styleInterpolate(name, i, priority);
      return t;
    }
    tween._value = value;
    return tween;
  }

  function transition_styleTween(name, value, priority) {
    var key = "style." + (name += "");
    if (arguments.length < 2) return (key = this.tween(key)) && key._value;
    if (value == null) return this.tween(key, null);
    if (typeof value !== "function") throw new Error;
    return this.tween(key, styleTween(name, value, priority == null ? "" : priority));
  }

  function textConstant$1(value) {
    return function() {
      this.textContent = value;
    };
  }

  function textFunction$1(value) {
    return function() {
      var value1 = value(this);
      this.textContent = value1 == null ? "" : value1;
    };
  }

  function transition_text(value) {
    return this.tween("text", typeof value === "function"
        ? textFunction$1(tweenValue(this, "text", value))
        : textConstant$1(value == null ? "" : value + ""));
  }

  function transition_transition() {
    var name = this._name,
        id0 = this._id,
        id1 = newId();

    for (var groups = this._groups, m = groups.length, j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
        if (node = group[i]) {
          var inherit = get$1(node, id0);
          schedule(node, name, id1, i, group, {
            time: inherit.time + inherit.delay + inherit.duration,
            delay: 0,
            duration: inherit.duration,
            ease: inherit.ease
          });
        }
      }
    }

    return new Transition(groups, this._parents, name, id1);
  }

  function transition_end() {
    var on0, on1, that = this, id = that._id, size = that.size();
    return new Promise(function(resolve, reject) {
      var cancel = {value: reject},
          end = {value: function() { if (--size === 0) resolve(); }};

      that.each(function() {
        var schedule = set$1(this, id),
            on = schedule.on;

        // If this node shared a dispatch with the previous node,
        // just assign the updated shared dispatch and we’re done!
        // Otherwise, copy-on-write.
        if (on !== on0) {
          on1 = (on0 = on).copy();
          on1._.cancel.push(cancel);
          on1._.interrupt.push(cancel);
          on1._.end.push(end);
        }

        schedule.on = on1;
      });
    });
  }

  var id = 0;

  function Transition(groups, parents, name, id) {
    this._groups = groups;
    this._parents = parents;
    this._name = name;
    this._id = id;
  }

  function transition(name) {
    return selection().transition(name);
  }

  function newId() {
    return ++id;
  }

  var selection_prototype = selection.prototype;

  Transition.prototype = transition.prototype = {
    constructor: Transition,
    select: transition_select,
    selectAll: transition_selectAll,
    filter: transition_filter,
    merge: transition_merge,
    selection: transition_selection,
    transition: transition_transition,
    call: selection_prototype.call,
    nodes: selection_prototype.nodes,
    node: selection_prototype.node,
    size: selection_prototype.size,
    empty: selection_prototype.empty,
    each: selection_prototype.each,
    on: transition_on,
    attr: transition_attr,
    attrTween: transition_attrTween,
    style: transition_style,
    styleTween: transition_styleTween,
    text: transition_text,
    remove: transition_remove,
    tween: transition_tween,
    delay: transition_delay,
    duration: transition_duration,
    ease: transition_ease,
    end: transition_end
  };

  function cubicInOut(t) {
    return ((t *= 2) <= 1 ? t * t * t : (t -= 2) * t * t + 2) / 2;
  }

  var defaultTiming = {
    time: null, // Set on use.
    delay: 0,
    duration: 250,
    ease: cubicInOut
  };

  function inherit(node, id) {
    var timing;
    while (!(timing = node.__transition) || !(timing = timing[id])) {
      if (!(node = node.parentNode)) {
        return defaultTiming.time = now(), defaultTiming;
      }
    }
    return timing;
  }

  function selection_transition(name) {
    var id,
        timing;

    if (name instanceof Transition) {
      id = name._id, name = name._name;
    } else {
      id = newId(), (timing = defaultTiming).time = now(), name = name == null ? null : name + "";
    }

    for (var groups = this._groups, m = groups.length, j = 0; j < m; ++j) {
      for (var group = groups[j], n = group.length, node, i = 0; i < n; ++i) {
        if (node = group[i]) {
          schedule(node, name, id, i, group, timing || inherit(node, id));
        }
      }
    }

    return new Transition(groups, this._parents, name, id);
  }

  selection.prototype.interrupt = selection_interrupt;
  selection.prototype.transition = selection_transition;

  var prefix = "$";

  function Map() {}

  Map.prototype = map.prototype = {
    constructor: Map,
    has: function(key) {
      return (prefix + key) in this;
    },
    get: function(key) {
      return this[prefix + key];
    },
    set: function(key, value) {
      this[prefix + key] = value;
      return this;
    },
    remove: function(key) {
      var property = prefix + key;
      return property in this && delete this[property];
    },
    clear: function() {
      for (var property in this) if (property[0] === prefix) delete this[property];
    },
    keys: function() {
      var keys = [];
      for (var property in this) if (property[0] === prefix) keys.push(property.slice(1));
      return keys;
    },
    values: function() {
      var values = [];
      for (var property in this) if (property[0] === prefix) values.push(this[property]);
      return values;
    },
    entries: function() {
      var entries = [];
      for (var property in this) if (property[0] === prefix) entries.push({key: property.slice(1), value: this[property]});
      return entries;
    },
    size: function() {
      var size = 0;
      for (var property in this) if (property[0] === prefix) ++size;
      return size;
    },
    empty: function() {
      for (var property in this) if (property[0] === prefix) return false;
      return true;
    },
    each: function(f) {
      for (var property in this) if (property[0] === prefix) f(this[property], property.slice(1), this);
    }
  };

  function map(object, f) {
    var map = new Map;

    // Copy constructor.
    if (object instanceof Map) object.each(function(value, key) { map.set(key, value); });

    // Index array by numeric index or specified key function.
    else if (Array.isArray(object)) {
      var i = -1,
          n = object.length,
          o;

      if (f == null) while (++i < n) map.set(i, object[i]);
      else while (++i < n) map.set(f(o = object[i], i, object), o);
    }

    // Convert object to map.
    else if (object) for (var key in object) map.set(key, object[key]);

    return map;
  }

  function nest() {
    var keys = [],
        sortKeys = [],
        sortValues,
        rollup,
        nest;

    function apply(array, depth, createResult, setResult) {
      if (depth >= keys.length) {
        if (sortValues != null) array.sort(sortValues);
        return rollup != null ? rollup(array) : array;
      }

      var i = -1,
          n = array.length,
          key = keys[depth++],
          keyValue,
          value,
          valuesByKey = map(),
          values,
          result = createResult();

      while (++i < n) {
        if (values = valuesByKey.get(keyValue = key(value = array[i]) + "")) {
          values.push(value);
        } else {
          valuesByKey.set(keyValue, [value]);
        }
      }

      valuesByKey.each(function(values, key) {
        setResult(result, key, apply(values, depth, createResult, setResult));
      });

      return result;
    }

    function entries(map, depth) {
      if (++depth > keys.length) return map;
      var array, sortKey = sortKeys[depth - 1];
      if (rollup != null && depth >= keys.length) array = map.entries();
      else array = [], map.each(function(v, k) { array.push({key: k, values: entries(v, depth)}); });
      return sortKey != null ? array.sort(function(a, b) { return sortKey(a.key, b.key); }) : array;
    }

    return nest = {
      object: function(array) { return apply(array, 0, createObject, setObject); },
      map: function(array) { return apply(array, 0, createMap, setMap); },
      entries: function(array) { return entries(apply(array, 0, createMap, setMap), 0); },
      key: function(d) { keys.push(d); return nest; },
      sortKeys: function(order) { sortKeys[keys.length - 1] = order; return nest; },
      sortValues: function(order) { sortValues = order; return nest; },
      rollup: function(f) { rollup = f; return nest; }
    };
  }

  function createObject() {
    return {};
  }

  function setObject(object, key, value) {
    object[key] = value;
  }

  function createMap() {
    return map();
  }

  function setMap(map, key, value) {
    map.set(key, value);
  }

  function Set() {}

  var proto = map.prototype;

  Set.prototype = set$2.prototype = {
    constructor: Set,
    has: proto.has,
    add: function(value) {
      value += "";
      this[prefix + value] = value;
      return this;
    },
    remove: proto.remove,
    clear: proto.clear,
    values: proto.keys,
    size: proto.size,
    empty: proto.empty,
    each: proto.each
  };

  function set$2(object, f) {
    var set = new Set;

    // Copy constructor.
    if (object instanceof Set) object.each(function(value) { set.add(value); });

    // Otherwise, assume it’s an array.
    else if (object) {
      var i = -1, n = object.length;
      if (f == null) while (++i < n) set.add(object[i]);
      else while (++i < n) set.add(f(object[i], i, object));
    }

    return set;
  }

  var EOL = {},
      EOF = {},
      QUOTE = 34,
      NEWLINE = 10,
      RETURN = 13;

  function objectConverter(columns) {
    return new Function("d", "return {" + columns.map(function(name, i) {
      return JSON.stringify(name) + ": d[" + i + "]";
    }).join(",") + "}");
  }

  function customConverter(columns, f) {
    var object = objectConverter(columns);
    return function(row, i) {
      return f(object(row), i, columns);
    };
  }

  // Compute unique columns in order of discovery.
  function inferColumns(rows) {
    var columnSet = Object.create(null),
        columns = [];

    rows.forEach(function(row) {
      for (var column in row) {
        if (!(column in columnSet)) {
          columns.push(columnSet[column] = column);
        }
      }
    });

    return columns;
  }

  function pad(value, width) {
    var s = value + "", length = s.length;
    return length < width ? new Array(width - length + 1).join(0) + s : s;
  }

  function formatYear(year) {
    return year < 0 ? "-" + pad(-year, 6)
      : year > 9999 ? "+" + pad(year, 6)
      : pad(year, 4);
  }

  function formatDate(date) {
    var hours = date.getUTCHours(),
        minutes = date.getUTCMinutes(),
        seconds = date.getUTCSeconds(),
        milliseconds = date.getUTCMilliseconds();
    return isNaN(date) ? "Invalid Date"
        : formatYear(date.getUTCFullYear()) + "-" + pad(date.getUTCMonth() + 1, 2) + "-" + pad(date.getUTCDate(), 2)
        + (milliseconds ? "T" + pad(hours, 2) + ":" + pad(minutes, 2) + ":" + pad(seconds, 2) + "." + pad(milliseconds, 3) + "Z"
        : seconds ? "T" + pad(hours, 2) + ":" + pad(minutes, 2) + ":" + pad(seconds, 2) + "Z"
        : minutes || hours ? "T" + pad(hours, 2) + ":" + pad(minutes, 2) + "Z"
        : "");
  }

  function dsvFormat(delimiter) {
    var reFormat = new RegExp("[\"" + delimiter + "\n\r]"),
        DELIMITER = delimiter.charCodeAt(0);

    function parse(text, f) {
      var convert, columns, rows = parseRows(text, function(row, i) {
        if (convert) return convert(row, i - 1);
        columns = row, convert = f ? customConverter(row, f) : objectConverter(row);
      });
      rows.columns = columns || [];
      return rows;
    }

    function parseRows(text, f) {
      var rows = [], // output rows
          N = text.length,
          I = 0, // current character index
          n = 0, // current line number
          t, // current token
          eof = N <= 0, // current token followed by EOF?
          eol = false; // current token followed by EOL?

      // Strip the trailing newline.
      if (text.charCodeAt(N - 1) === NEWLINE) --N;
      if (text.charCodeAt(N - 1) === RETURN) --N;

      function token() {
        if (eof) return EOF;
        if (eol) return eol = false, EOL;

        // Unescape quotes.
        var i, j = I, c;
        if (text.charCodeAt(j) === QUOTE) {
          while (I++ < N && text.charCodeAt(I) !== QUOTE || text.charCodeAt(++I) === QUOTE);
          if ((i = I) >= N) eof = true;
          else if ((c = text.charCodeAt(I++)) === NEWLINE) eol = true;
          else if (c === RETURN) { eol = true; if (text.charCodeAt(I) === NEWLINE) ++I; }
          return text.slice(j + 1, i - 1).replace(/""/g, "\"");
        }

        // Find next delimiter or newline.
        while (I < N) {
          if ((c = text.charCodeAt(i = I++)) === NEWLINE) eol = true;
          else if (c === RETURN) { eol = true; if (text.charCodeAt(I) === NEWLINE) ++I; }
          else if (c !== DELIMITER) continue;
          return text.slice(j, i);
        }

        // Return last token before EOF.
        return eof = true, text.slice(j, N);
      }

      while ((t = token()) !== EOF) {
        var row = [];
        while (t !== EOL && t !== EOF) row.push(t), t = token();
        if (f && (row = f(row, n++)) == null) continue;
        rows.push(row);
      }

      return rows;
    }

    function preformatBody(rows, columns) {
      return rows.map(function(row) {
        return columns.map(function(column) {
          return formatValue(row[column]);
        }).join(delimiter);
      });
    }

    function format(rows, columns) {
      if (columns == null) columns = inferColumns(rows);
      return [columns.map(formatValue).join(delimiter)].concat(preformatBody(rows, columns)).join("\n");
    }

    function formatBody(rows, columns) {
      if (columns == null) columns = inferColumns(rows);
      return preformatBody(rows, columns).join("\n");
    }

    function formatRows(rows) {
      return rows.map(formatRow).join("\n");
    }

    function formatRow(row) {
      return row.map(formatValue).join(delimiter);
    }

    function formatValue(value) {
      return value == null ? ""
          : value instanceof Date ? formatDate(value)
          : reFormat.test(value += "") ? "\"" + value.replace(/"/g, "\"\"") + "\""
          : value;
    }

    return {
      parse: parse,
      parseRows: parseRows,
      format: format,
      formatBody: formatBody,
      formatRows: formatRows
    };
  }

  var csv = dsvFormat(",");

  var tsv = dsvFormat("\t");

  function responseJson(response) {
    if (!response.ok) throw new Error(response.status + " " + response.statusText);
    return response.json();
  }

  function json(input, init) {
    return fetch(input, init).then(responseJson);
  }

  var warm = cubehelixLong(cubehelix(-100, 0.75, 0.35), cubehelix(80, 1.50, 0.8));

  var cool = cubehelixLong(cubehelix(260, 0.75, 0.35), cubehelix(80, 1.50, 0.8));

  var c = cubehelix();

  function count(node) {
    var sum = 0,
        children = node.children,
        i = children && children.length;
    if (!i) sum = 1;
    else while (--i >= 0) sum += children[i].value;
    node.value = sum;
  }

  function node_count() {
    return this.eachAfter(count);
  }

  function node_each(callback) {
    var node = this, current, next = [node], children, i, n;
    do {
      current = next.reverse(), next = [];
      while (node = current.pop()) {
        callback(node), children = node.children;
        if (children) for (i = 0, n = children.length; i < n; ++i) {
          next.push(children[i]);
        }
      }
    } while (next.length);
    return this;
  }

  function node_eachBefore(callback) {
    var node = this, nodes = [node], children, i;
    while (node = nodes.pop()) {
      callback(node), children = node.children;
      if (children) for (i = children.length - 1; i >= 0; --i) {
        nodes.push(children[i]);
      }
    }
    return this;
  }

  function node_eachAfter(callback) {
    var node = this, nodes = [node], next = [], children, i, n;
    while (node = nodes.pop()) {
      next.push(node), children = node.children;
      if (children) for (i = 0, n = children.length; i < n; ++i) {
        nodes.push(children[i]);
      }
    }
    while (node = next.pop()) {
      callback(node);
    }
    return this;
  }

  function node_sum(value) {
    return this.eachAfter(function(node) {
      var sum = +value(node.data) || 0,
          children = node.children,
          i = children && children.length;
      while (--i >= 0) sum += children[i].value;
      node.value = sum;
    });
  }

  function node_sort(compare) {
    return this.eachBefore(function(node) {
      if (node.children) {
        node.children.sort(compare);
      }
    });
  }

  function node_path(end) {
    var start = this,
        ancestor = leastCommonAncestor(start, end),
        nodes = [start];
    while (start !== ancestor) {
      start = start.parent;
      nodes.push(start);
    }
    var k = nodes.length;
    while (end !== ancestor) {
      nodes.splice(k, 0, end);
      end = end.parent;
    }
    return nodes;
  }

  function leastCommonAncestor(a, b) {
    if (a === b) return a;
    var aNodes = a.ancestors(),
        bNodes = b.ancestors(),
        c = null;
    a = aNodes.pop();
    b = bNodes.pop();
    while (a === b) {
      c = a;
      a = aNodes.pop();
      b = bNodes.pop();
    }
    return c;
  }

  function node_ancestors() {
    var node = this, nodes = [node];
    while (node = node.parent) {
      nodes.push(node);
    }
    return nodes;
  }

  function node_descendants() {
    var nodes = [];
    this.each(function(node) {
      nodes.push(node);
    });
    return nodes;
  }

  function node_leaves() {
    var leaves = [];
    this.eachBefore(function(node) {
      if (!node.children) {
        leaves.push(node);
      }
    });
    return leaves;
  }

  function node_links() {
    var root = this, links = [];
    root.each(function(node) {
      if (node !== root) { // Don’t include the root’s parent, if any.
        links.push({source: node.parent, target: node});
      }
    });
    return links;
  }

  function hierarchy(data, children) {
    var root = new Node(data),
        valued = +data.value && (root.value = data.value),
        node,
        nodes = [root],
        child,
        childs,
        i,
        n;

    if (children == null) children = defaultChildren;

    while (node = nodes.pop()) {
      if (valued) node.value = +node.data.value;
      if ((childs = children(node.data)) && (n = childs.length)) {
        node.children = new Array(n);
        for (i = n - 1; i >= 0; --i) {
          nodes.push(child = node.children[i] = new Node(childs[i]));
          child.parent = node;
          child.depth = node.depth + 1;
        }
      }
    }

    return root.eachBefore(computeHeight);
  }

  function node_copy() {
    return hierarchy(this).eachBefore(copyData);
  }

  function defaultChildren(d) {
    return d.children;
  }

  function copyData(node) {
    node.data = node.data.data;
  }

  function computeHeight(node) {
    var height = 0;
    do node.height = height;
    while ((node = node.parent) && (node.height < ++height));
  }

  function Node(data) {
    this.data = data;
    this.depth =
    this.height = 0;
    this.parent = null;
  }

  Node.prototype = hierarchy.prototype = {
    constructor: Node,
    count: node_count,
    each: node_each,
    eachAfter: node_eachAfter,
    eachBefore: node_eachBefore,
    sum: node_sum,
    sort: node_sort,
    path: node_path,
    ancestors: node_ancestors,
    descendants: node_descendants,
    leaves: node_leaves,
    links: node_links,
    copy: node_copy
  };

  function Rectangle(x, y, w, h) { // new Rectangle(bounds) or new Rectangle(x, y, w, h)
    if (!(this instanceof Rectangle)) {
      return new Rectangle(x, y, w, h);
    }
    var x2, y2, p;

    if (x.x) {
      w = x.w;
      h = x.h;
      y = x.y;
      if (x.w !== 0 && !x.w && x.x2) {
        w = x.x2 - x.x;
        h = x.y2 - x.y;
      }
      else {
        w = x.w;
        h = x.h;
      }
      x = x.x;
      // For extra fastitude
      x2 = x + w;
      y2 = y + h;
      p = (h + w) ? false : true;
    }
    else {
      // For extra fastitude
      x2 = x + w;
      y2 = y + h;
      p = (h + w) ? false : true;
    }

    this.x1 = this.x = function () {
      return x;
    };
    this.y1 = this.y = function () {
      return y;
    };
    this.x2 = function () {
      return x2;
    };
    this.y2 = function () {
      return y2;
    };
    this.w = function () {
      return w;
    };
    this.h = function () {
      return h;
    };
    this.p = function () {
      return p;
    };

    this.overlap = function (a) {
      if (p || a.p()) {
        return x <= a.x2() && x2 >= a.x() && y <= a.y2() && y2 >= a.y();
      }
      return x < a.x2() && x2 > a.x() && y < a.y2() && y2 > a.y();
    };

    this.expand = function (a) {
      var nx, ny;
      var ax = a.x();
      var ay = a.y();
      var ax2 = a.x2();
      var ay2 = a.y2();
      if (x > ax) {
        nx = ax;
      }
      else {
        nx = x;
      }
      if (y > ay) {
        ny = ay;
      }
      else {
        ny = y;
      }
      if (x2 > ax2) {
        w = x2 - nx;
      }
      else {
        w = ax2 - nx;
      }
      if (y2 > ay2) {
        h = y2 - ny;
      }
      else {
        h = ay2 - ny;
      }
      x = nx;
      y = ny;
      return this;
    };

    //End of RTree.Rectangle
  }


  /* returns true if rectangle 1 overlaps rectangle 2
   * [ boolean ] = overlapRectangle(rectangle a, rectangle b)
   * @static function
   */
  Rectangle.overlapRectangle = function (a, b) {
    //if(!((a.h||a.w)&&(b.h||b.w))){ not faster resist the urge!
    if ((a.h === 0 && a.w === 0) || (b.h === 0 && b.w === 0)) {
      return a.x <= (b.x + b.w) && (a.x + a.w) >= b.x && a.y <= (b.y + b.h) && (a.y + a.h) >= b.y;
    }
    else {
      return a.x < (b.x + b.w) && (a.x + a.w) > b.x && a.y < (b.y + b.h) && (a.y + a.h) > b.y;
    }
  };

  /* returns true if rectangle a is contained in rectangle b
   * [ boolean ] = containsRectangle(rectangle a, rectangle b)
   * @static function
   */
  Rectangle.containsRectangle = function (a, b) {
    return (a.x + a.w) <= (b.x + b.w) && a.x >= b.x && (a.y + a.h) <= (b.y + b.h) && a.y >= b.y;
  };

  /* expands rectangle A to include rectangle B, rectangle B is untouched
   * [ rectangle a ] = expandRectangle(rectangle a, rectangle b)
   * @static function
   */
  Rectangle.expandRectangle = function (a, b) {
    var nx, ny;
    var axw = a.x + a.w;
    var bxw = b.x + b.w;
    var ayh = a.y + a.h;
    var byh = b.y + b.h;
    if (a.x > b.x) {
      nx = b.x;
    }
    else {
      nx = a.x;
    }
    if (a.y > b.y) {
      ny = b.y;
    }
    else {
      ny = a.y;
    }
    if (axw > bxw) {
      a.w = axw - nx;
    }
    else {
      a.w = bxw - nx;
    }
    if (ayh > byh) {
      a.h = ayh - ny;
    }
    else {
      a.h = byh - ny;
    }
    a.x = nx;
    a.y = ny;
    return a;
  };

  /* generates a minimally bounding rectangle for all rectangles in
   * array 'nodes'. If rect is set, it is modified into the MBR. Otherwise,
   * a new rectangle is generated and returned.
   * [ rectangle a ] = makeMBR(rectangle array nodes, rectangle rect)
   * @static function
   */
  Rectangle.makeMBR = function (nodes, rect) {
    if (!nodes.length) {
      return {
        x: 0,
        y: 0,
        w: 0,
        h: 0
      };
    }
    rect = rect || {};
    rect.x = nodes[0].x;
    rect.y = nodes[0].y;
    rect.w = nodes[0].w;
    rect.h = nodes[0].h;

    for (var i = 1, len = nodes.length; i < len; i++) {
      Rectangle.expandRectangle(rect, nodes[i]);
    }

    return rect;
  };
  Rectangle.squarifiedRatio = function (l, w, fill) {
    // Area of new enlarged rectangle
    var lperi = (l + w) / 2.0; // Average size of a side of the new rectangle
    var larea = l * w; // Area of new rectangle
    // return the ratio of the perimeter to the area - the closer to 1 we are,
    // the more 'square' a rectangle is. conversly, when approaching zero the
    // more elongated a rectangle is
    var lgeo = larea / (lperi * lperi);
    return larea * fill / lgeo;
  };
  var rectangle = Rectangle;

  function RTree(width) {
    if (!(this instanceof RTree)) {
      return new RTree(width);
    }
    // Variables to control tree-dimensions
    var minWidth = 3;  // Minimum width of any node before a merge
    var maxWidth = 6;  // Maximum width of any node before a split
    if (!isNaN(width)) {
      minWidth = Math.floor(width / 2.0);
      maxWidth = width;
    }
    // Start with an empty root-tree
    var rootTree = {x: 0, y: 0, w: 0, h: 0, id: 'root', nodes: [] };
    this.root = rootTree;


    // This is my special addition to the world of r-trees
    // every other (simple) method I found produced crap trees
    // this skews insertions to prefering squarer and emptier nodes
    var flatten = function (tree) {
      var todo = tree.slice();
      var done = [];
      var current;
      while (todo.length) {
        current = todo.pop();
        if (current.nodes) {
          todo = todo.concat(current.nodes);
        } else if (current.leaf) {
          done.push(current);
        }
      }
      return done;
    };
    /* find the best specific node(s) for object to be deleted from
     * [ leaf node parent ] = removeSubtree(rectangle, object, root)
     * @private
     */
    var removeSubtree = function (rect, obj, root) {
      var hitStack = []; // Contains the elements that overlap
      var countStack = []; // Contains the elements that overlap
      var retArray = [];
      var tree, i, ltree;
      if (!rect || !rectangle.overlapRectangle(rect, root)) {
        return retArray;
      }
      var retObj = {x: rect.x, y: rect.y, w: rect.w, h: rect.h, target: obj};

      countStack.push(root.nodes.length);
      hitStack.push(root);
      while (hitStack.length > 0) {
        tree = hitStack.pop();
        i = countStack.pop() - 1;
        if ('target' in retObj) { // will this ever be false?
          while (i >= 0) {
            ltree = tree.nodes[i];
            if (rectangle.overlapRectangle(retObj, ltree)) {
              if ((retObj.target && 'leaf' in ltree && ltree.leaf === retObj.target) || (!retObj.target && ('leaf' in ltree || rectangle.containsRectangle(ltree, retObj)))) {
                // A Match !!
              // Yup we found a match...
              // we can cancel search and start walking up the list
                if ('nodes' in ltree) {// If we are deleting a node not a leaf...
                  retArray = flatten(tree.nodes.splice(i, 1));
                } else {
                  retArray = tree.nodes.splice(i, 1);
                }
                // Resize MBR down...
                rectangle.makeMBR(tree.nodes, tree);
                delete retObj.target;
                //if (tree.nodes.length < minWidth) { // Underflow
                //  retObj.nodes = searchSubtree(tree, true, [], tree);
                //}
                break;
              } else if ('nodes' in ltree) { // Not a Leaf
                countStack.push(i);
                hitStack.push(tree);
                tree = ltree;
                i = ltree.nodes.length;
              }
            }
            i--;
          }

        } else if ('nodes' in retObj) { // We are unsplitting

          tree.nodes.splice(i + 1, 1); // Remove unsplit node
          if (tree.nodes.length > 0) {
            rectangle.makeMBR(tree.nodes, tree);
          }
          for (var t = 0;t < retObj.nodes.length;t++) {
            insertSubtree(retObj.nodes[t], tree);
          }
          retObj.nodes = [];
          if (hitStack.length === 0 && tree.nodes.length <= 1) { // Underflow..on root!
            retObj.nodes = searchSubtree(tree, true, retObj.nodes, tree);
            tree.nodes = [];
            hitStack.push(tree);
            countStack.push(1);
          } else if (hitStack.length > 0 && tree.nodes.length < minWidth) { // Underflow..AGAIN!
            retObj.nodes = searchSubtree(tree, true, retObj.nodes, tree);
            tree.nodes = [];
          } else {
            delete retObj.nodes; // Just start resizing
          }
        } else { // we are just resizing
          rectangle.makeMBR(tree.nodes, tree);
        }
      }
      return retArray;
    };

    /* choose the best damn node for rectangle to be inserted into
     * [ leaf node parent ] = chooseLeafSubtree(rectangle, root to start search at)
     * @private
     */
    var chooseLeafSubtree = function (rect, root) {
      var bestChoiceIndex = -1;
      var bestChoiceStack = [];
      var bestChoiceArea;
      var first = true;
      bestChoiceStack.push(root);
      var nodes = root.nodes;

      while (first || bestChoiceIndex !== -1) {
        if (first) {
          first = false;
        } else {
          bestChoiceStack.push(nodes[bestChoiceIndex]);
          nodes = nodes[bestChoiceIndex].nodes;
          bestChoiceIndex = -1;
        }

        for (var i = nodes.length - 1; i >= 0; i--) {
          var ltree = nodes[i];
          if ('leaf' in ltree) {
            // Bail out of everything and start inserting
            bestChoiceIndex = -1;
            break;
          }
          // Area of new enlarged rectangle
          var oldLRatio = rectangle.squarifiedRatio(ltree.w, ltree.h, ltree.nodes.length + 1);

          // Enlarge rectangle to fit new rectangle
          var nw = Math.max(ltree.x + ltree.w, rect.x + rect.w) - Math.min(ltree.x, rect.x);
          var nh = Math.max(ltree.y + ltree.h, rect.y + rect.h) - Math.min(ltree.y, rect.y);

          // Area of new enlarged rectangle
          var lratio = rectangle.squarifiedRatio(nw, nh, ltree.nodes.length + 2);

          if (bestChoiceIndex < 0 || Math.abs(lratio - oldLRatio) < bestChoiceArea) {
            bestChoiceArea = Math.abs(lratio - oldLRatio);
            bestChoiceIndex = i;
          }
        }
      }

      return bestChoiceStack;
    };

    /* split a set of nodes into two roughly equally-filled nodes
     * [ an array of two new arrays of nodes ] = linearSplit(array of nodes)
     * @private
     */
    var linearSplit = function (nodes) {
      var n = pickLinear(nodes);
      while (nodes.length > 0) {
        pickNext(nodes, n[0], n[1]);
      }
      return n;
    };

    /* insert the best source rectangle into the best fitting parent node: a or b
     * [] = pickNext(array of source nodes, target node array a, target node array b)
     * @private
     */
    var pickNext = function (nodes, a, b) {
    // Area of new enlarged rectangle
      var areaA = rectangle.squarifiedRatio(a.w, a.h, a.nodes.length + 1);
      var areaB = rectangle.squarifiedRatio(b.w, b.h, b.nodes.length + 1);
      var highAreaDelta;
      var highAreaNode;
      var lowestGrowthGroup;

      for (var i = nodes.length - 1; i >= 0;i--) {
        var l = nodes[i];
        var newAreaA = {};
        newAreaA.x = Math.min(a.x, l.x);
        newAreaA.y = Math.min(a.y, l.y);
        newAreaA.w = Math.max(a.x + a.w, l.x + l.w) - newAreaA.x;
        newAreaA.h = Math.max(a.y + a.h, l.y + l.h) - newAreaA.y;
        var changeNewAreaA = Math.abs(rectangle.squarifiedRatio(newAreaA.w, newAreaA.h, a.nodes.length + 2) - areaA);

        var newAreaB = {};
        newAreaB.x = Math.min(b.x, l.x);
        newAreaB.y = Math.min(b.y, l.y);
        newAreaB.w = Math.max(b.x + b.w, l.x + l.w) - newAreaB.x;
        newAreaB.h = Math.max(b.y + b.h, l.y + l.h) - newAreaB.y;
        var changeNewAreaB = Math.abs(rectangle.squarifiedRatio(newAreaB.w, newAreaB.h, b.nodes.length + 2) - areaB);

        if (!highAreaNode || !highAreaDelta || Math.abs(changeNewAreaB - changeNewAreaA) < highAreaDelta) {
          highAreaNode = i;
          highAreaDelta = Math.abs(changeNewAreaB - changeNewAreaA);
          lowestGrowthGroup = changeNewAreaB < changeNewAreaA ? b : a;
        }
      }
      var tempNode = nodes.splice(highAreaNode, 1)[0];
      if (a.nodes.length + nodes.length + 1 <= minWidth) {
        a.nodes.push(tempNode);
        rectangle.expandRectangle(a, tempNode);
      }  else if (b.nodes.length + nodes.length + 1 <= minWidth) {
        b.nodes.push(tempNode);
        rectangle.expandRectangle(b, tempNode);
      }
      else {
        lowestGrowthGroup.nodes.push(tempNode);
        rectangle.expandRectangle(lowestGrowthGroup, tempNode);
      }
    };

    /* pick the 'best' two starter nodes to use as seeds using the 'linear' criteria
     * [ an array of two new arrays of nodes ] = pickLinear(array of source nodes)
     * @private
     */
    var pickLinear = function (nodes) {
      var lowestHighX = nodes.length - 1;
      var highestLowX = 0;
      var lowestHighY = nodes.length - 1;
      var highestLowY = 0;
      var t1, t2;

      for (var i = nodes.length - 2; i >= 0;i--) {
        var l = nodes[i];
        if (l.x > nodes[highestLowX].x) {
          highestLowX = i;
        } else if (l.x + l.w < nodes[lowestHighX].x + nodes[lowestHighX].w) {
          lowestHighX = i;
        }
        if (l.y > nodes[highestLowY].y) {
          highestLowY = i;
        } else if (l.y + l.h < nodes[lowestHighY].y + nodes[lowestHighY].h) {
          lowestHighY = i;
        }
      }
      var dx = Math.abs((nodes[lowestHighX].x + nodes[lowestHighX].w) - nodes[highestLowX].x);
      var dy = Math.abs((nodes[lowestHighY].y + nodes[lowestHighY].h) - nodes[highestLowY].y);
      if (dx > dy)  {
        if (lowestHighX > highestLowX)  {
          t1 = nodes.splice(lowestHighX, 1)[0];
          t2 = nodes.splice(highestLowX, 1)[0];
        }  else {
          t2 = nodes.splice(highestLowX, 1)[0];
          t1 = nodes.splice(lowestHighX, 1)[0];
        }
      }  else {
        if (lowestHighY > highestLowY)  {
          t1 = nodes.splice(lowestHighY, 1)[0];
          t2 = nodes.splice(highestLowY, 1)[0];
        }  else {
          t2 = nodes.splice(highestLowY, 1)[0];
          t1 = nodes.splice(lowestHighY, 1)[0];
        }
      }
      return [
        {x: t1.x, y: t1.y, w: t1.w, h: t1.h, nodes: [t1]},
        {x: t2.x, y: t2.y, w: t2.w, h: t2.h, nodes: [t2]}
      ];
    };

    var attachData = function (node, moreTree) {
      node.nodes = moreTree.nodes;
      node.x = moreTree.x;
      node.y = moreTree.y;
      node.w = moreTree.w;
      node.h = moreTree.h;
      return node;
    };

    /* non-recursive internal search function
    * [ nodes | objects ] = searchSubtree(rectangle, [return node data], [array to fill], root to begin search at)
     * @private
     */
    var searchSubtree = function (rect, returnNode, returnArray, root) {
      var hitStack = []; // Contains the elements that overlap

      if (!rectangle.overlapRectangle(rect, root)) {
        return returnArray;
      }


      hitStack.push(root.nodes);

      while (hitStack.length > 0) {
        var nodes = hitStack.pop();

        for (var i = nodes.length - 1; i >= 0; i--) {
          var ltree = nodes[i];
          if (rectangle.overlapRectangle(rect, ltree)) {
            if ('nodes' in ltree) { // Not a Leaf
              hitStack.push(ltree.nodes);
            } else if ('leaf' in ltree) { // A Leaf !!
              if (!returnNode) {
                returnArray.push(ltree.leaf);
              } else {
                returnArray.push(ltree);
              }
            }
          }
        }
      }

      return returnArray;
    };

    /* non-recursive internal insert function
     * [] = insertSubtree(rectangle, object to insert, root to begin insertion at)
     * @private
     */
    var insertSubtree = function (node, root) {
      var bc; // Best Current node
      // Initial insertion is special because we resize the Tree and we don't
      // care about any overflow (seriously, how can the first object overflow?)
      if (root.nodes.length === 0) {
        root.x = node.x;
        root.y = node.y;
        root.w = node.w;
        root.h = node.h;
        root.nodes.push(node);
        return;
      }

      // Find the best fitting leaf node
      // chooseLeaf returns an array of all tree levels (including root)
      // that were traversed while trying to find the leaf
      var treeStack = chooseLeafSubtree(node, root);
      var retObj = node;//{x:rect.x,y:rect.y,w:rect.w,h:rect.h, leaf:obj};
      var pbc;
      // Walk back up the tree resizing and inserting as needed
      while (treeStack.length > 0) {
        //handle the case of an empty node (from a split)
        if (bc && 'nodes' in bc && bc.nodes.length === 0) {
          pbc = bc; // Past bc
          bc = treeStack.pop();
          for (var t = 0;t < bc.nodes.length;t++) {
            if (bc.nodes[t] === pbc || bc.nodes[t].nodes.length === 0) {
              bc.nodes.splice(t, 1);
              break;
            }
          }
        } else {
          bc = treeStack.pop();
        }

        // If there is data attached to this retObj
        if ('leaf' in retObj || 'nodes' in retObj || Array.isArray(retObj)) {
          // Do Insert
          if (Array.isArray(retObj)) {
            for (var ai = 0; ai < retObj.length; ai++) {
              rectangle.expandRectangle(bc, retObj[ai]);
            }
            bc.nodes = bc.nodes.concat(retObj);
          } else {
            rectangle.expandRectangle(bc, retObj);
            bc.nodes.push(retObj); // Do Insert
          }

          if (bc.nodes.length <= maxWidth)  { // Start Resizeing Up the Tree
            retObj = {x: bc.x, y: bc.y, w: bc.w, h: bc.h};
          }  else { // Otherwise Split this Node
            // linearSplit() returns an array containing two new nodes
            // formed from the split of the previous node's overflow
            var a = linearSplit(bc.nodes);
            retObj = a;//[1];

            if (treeStack.length < 1)  { // If are splitting the root..
              bc.nodes.push(a[0]);
              treeStack.push(bc);  // Reconsider the root element
              retObj = a[1];
            } /*else {
              delete bc;
            }*/
          }
        } else { // Otherwise Do Resize
          //Just keep applying the new bounding rectangle to the parents..
          rectangle.expandRectangle(bc, retObj);
          retObj = {x: bc.x, y: bc.y, w: bc.w, h: bc.h};
        }
      }
    };

    this.insertSubtree = insertSubtree;
    /* quick 'n' dirty function for plugins or manually drawing the tree
     * [ tree ] = RTree.getTree(): returns the raw tree data. useful for adding
     * @public
     * !! DEPRECATED !!
     */
    this.getTree = function () {
      return rootTree;
    };

    /* quick 'n' dirty function for plugins or manually loading the tree
     * [ tree ] = RTree.setTree(sub-tree, where to attach): returns the raw tree data. useful for adding
     * @public
     * !! DEPRECATED !!
     */
    this.setTree = function (newTree, where) {
      if (!where) {
        where = rootTree;
      }
      return attachData(where, newTree);
    };

    /* non-recursive search function
    * [ nodes | objects ] = RTree.search(rectangle, [return node data], [array to fill])
     * @public
     */
    this.search = function (rect, returnNode, returnArray) {
      returnArray = returnArray || [];
      return searchSubtree(rect, returnNode, returnArray, rootTree);
    };


    var removeArea = function (rect) {
      var numberDeleted = 1,
      retArray = [],
      deleted;
      while (numberDeleted > 0) {
        deleted = removeSubtree(rect, false, rootTree);
        numberDeleted = deleted.length;
        retArray = retArray.concat(deleted);
      }
      return retArray;
    };

    var removeObj = function (rect, obj) {
      var retArray = removeSubtree(rect, obj, rootTree);
      return retArray;
    };
      /* non-recursive delete function
     * [deleted object] = RTree.remove(rectangle, [object to delete])
     */
    this.remove = function (rect, obj) {
      if (!obj || typeof obj === 'function') {
        return removeArea(rect);
      } else {
        return removeObj(rect, obj);
      }
    };

    /* non-recursive insert function
     * [] = RTree.insert(rectangle, object to insert)
     */
    this.insert = function (rect, obj) {
      var retArray = insertSubtree({x: rect.x, y: rect.y, w: rect.w, h: rect.h, leaf: obj}, rootTree);
      return retArray;
    };
  }
  RTree.prototype.toJSON = function (printing) {
    return JSON.stringify(this.root, false, printing);
  };

  RTree.fromJSON = function (json) {
    var rt = new RTree();
    rt.setTree(JSON.parse(json));
    return rt;
  };

  var rtree = RTree;


  /**
   * Polyfill for the Array.isArray function
   * todo: Test on IE7 and IE8
   * Taken from https://github.com/geraintluff/tv4/issues/20
   */
  if (typeof Array.isArray !== 'function') {
    Array.isArray = function (a) {
      return typeof a === 'object' && {}.toString.call(a) === '[object Array]';
    };
  }

  var bbox = function (ar, obj) {
    if (obj && obj.bbox) {
      return {
        leaf: obj,
        x: obj.bbox[0],
        y: obj.bbox[1],
        w: obj.bbox[2] - obj.bbox[0],
        h: obj.bbox[3] - obj.bbox[1]
      };
    }
    var len = ar.length;
    var i = 0;
    var a = new Array(len);
    while (i < len) {
      a[i] = [ar[i][0], ar[i][1]];
      i++;
    }
    var first = a[0];
    len = a.length;
    i = 1;
    var temp = {
      min: [].concat(first),
      max: [].concat(first)
    };
    while (i < len) {
      if (a[i][0] < temp.min[0]) {
        temp.min[0] = a[i][0];
      }
      else if (a[i][0] > temp.max[0]) {
        temp.max[0] = a[i][0];
      }
      if (a[i][1] < temp.min[1]) {
        temp.min[1] = a[i][1];
      }
      else if (a[i][1] > temp.max[1]) {
        temp.max[1] = a[i][1];
      }
      i++;
    }
    var out = {
      x: temp.min[0],
      y: temp.min[1],
      w: (temp.max[0] - temp.min[0]),
      h: (temp.max[1] - temp.min[1])
    };
    if (obj) {
      out.leaf = obj;
    }
    return out;
  };
  var geoJSON = {};
  geoJSON.point = function (obj, self) {
    return (self.insertSubtree({
      x: obj.geometry.coordinates[0],
      y: obj.geometry.coordinates[1],
      w: 0,
      h: 0,
      leaf: obj
    }, self.root));
  };
  geoJSON.multiPointLineString = function (obj, self) {
    return (self.insertSubtree(bbox(obj.geometry.coordinates, obj), self.root));
  };
  geoJSON.multiLineStringPolygon = function (obj, self) {
    return (self.insertSubtree(bbox(Array.prototype.concat.apply([], obj.geometry.coordinates), obj), self.root));
  };
  geoJSON.multiPolygon = function (obj, self) {
    return (self.insertSubtree(bbox(Array.prototype.concat.apply([], Array.prototype.concat.apply([], obj.geometry.coordinates)), obj), self.root));
  };
  geoJSON.makeRec = function (obj) {
    return rectangle(obj.x, obj.y, obj.w, obj.h);
  };
  geoJSON.geometryCollection = function (obj, self) {
    if (obj.bbox) {
      return (self.insertSubtree({
        leaf: obj,
        x: obj.bbox[0],
        y: obj.bbox[1],
        w: obj.bbox[2] - obj.bbox[0],
        h: obj.bbox[3] - obj.bbox[1]
      }, self.root));
    }
    var geos = obj.geometry.geometries;
    var i = 0;
    var len = geos.length;
    var temp = [];
    var g;
    while (i < len) {
      g = geos[i];
      switch (g.type) {
      case 'Point':
        temp.push(geoJSON.makeRec({
          x: g.coordinates[0],
          y: g.coordinates[1],
          w: 0,
          h: 0
        }));
        break;
      case 'MultiPoint':
        temp.push(geoJSON.makeRec(bbox(g.coordinates)));
        break;
      case 'LineString':
        temp.push(geoJSON.makeRec(bbox(g.coordinates)));
        break;
      case 'MultiLineString':
        temp.push(geoJSON.makeRec(bbox(Array.prototype.concat.apply([], g.coordinates))));
        break;
      case 'Polygon':
        temp.push(geoJSON.makeRec(bbox(Array.prototype.concat.apply([], g.coordinates))));
        break;
      case 'MultiPolygon':
        temp.push(geoJSON.makeRec(bbox(Array.prototype.concat.apply([], Array.prototype.concat.apply([], g.coordinates)))));
        break;
      case 'GeometryCollection':
        geos = geos.concat(g.geometries);
        len = geos.length;
        break;
      }
      i++;
    }
    var first = temp[0];
    i = 1;
    len = temp.length;
    while (i < len) {
      first.expand(temp[i]);
      i++;
    }
    return self.insertSubtree({
      leaf: obj,
      x: first.x(),
      y: first.y(),
      h: first.h(),
      w: first.w()
    }, self.root);
  };
  var geoJSON_1 = function (prelim) {
    var that = this;
    var features, feature;
    if (Array.isArray(prelim)) {
      features = prelim.slice();
    }
    else if (prelim.features && Array.isArray(prelim.features)) {
      features = prelim.features.slice();
    }
    else if (prelim instanceof Object) {
      features = [prelim];
    } else {
      throw ('this isn\'t what we\'re looking for');
    }
    var len = features.length;
    var i = 0;
    while (i < len) {
      feature = features[i];
      if (feature.type === 'Feature') {
        switch (feature.geometry.type) {
        case 'Point':
          geoJSON.point(feature, that);
          break;
        case 'MultiPoint':
          geoJSON.multiPointLineString(feature, that);
          break;
        case 'LineString':
          geoJSON.multiPointLineString(feature, that);
          break;
        case 'MultiLineString':
          geoJSON.multiLineStringPolygon(feature, that);
          break;
        case 'Polygon':
          geoJSON.multiLineStringPolygon(feature, that);
          break;
        case 'MultiPolygon':
          geoJSON.multiPolygon(feature, that);
          break;
        case 'GeometryCollection':
          geoJSON.geometryCollection(feature, that);
          break;
        }
      }
      i++;
    }
  };
  var bbox_1 = function () {
    var x1, y1, x2, y2;
    switch (arguments.length) {
    case 1:
      x1 = arguments[0][0][0];
      y1 = arguments[0][0][1];
      x2 = arguments[0][1][0];
      y2 = arguments[0][1][1];
      break;
    case 2:
      x1 = arguments[0][0];
      y1 = arguments[0][1];
      x2 = arguments[1][0];
      y2 = arguments[1][1];
      break;
    case 4:
      x1 = arguments[0];
      y1 = arguments[1];
      x2 = arguments[2];
      y2 = arguments[3];
      break;
    }

    return this.search({
      x: x1,
      y: y1,
      w: x2 - x1,
      h: y2 - y1
    });
  };

  var geojson = {
  	geoJSON: geoJSON_1,
  	bbox: bbox_1
  };

  rtree.prototype.bbox = geojson.bbox;
  rtree.prototype.geoJSON = geojson.geoJSON;
  rtree.Rectangle = rectangle;
  var lib = rtree;

  function phyllotaxis (x, y, outer_radius, item_radius) {
      // https://www.geeksforgeeks.org/algorithmic-botany-phyllotaxis-python/
      // https://observablehq.com/@mbostock/phyllotaxis

      function layout (items, numitems) {
          let l = items.length,
              a, r,
              da = Math.PI * (3 - Math.sqrt(5)); // (Math.PI * 2 * rings) / l,
          numitems = numitems || items.length;

          // layout with a spacing set to extend up to outer_radius
          // allow numitems to be provided that's maybe different from the actual
          // length of items

          // substracting the item_radius keeps the shapes inside the outer circle
          var spacing = (outer_radius-item_radius) / Math.sqrt(numitems+0.5);

          for (let i=0; i<l; i++) {
              a = i * da;
              r = spacing * Math.sqrt(i+0.5);
              items[i].x = x + (r*Math.cos(a));
              items[i].y = y - (r*Math.sin(a));
              items[i].r = item_radius;
              // a += da;
          }
      }
      return layout;
  }

  function arc_inscribe_circle (angle, outer_radius) {
      // takes an angle and outer_radius
      // and calculates:
      // b: the distance from the arc origin to the center of the
      //    inscribed circle
      // r: the radius of the circle
      // nb: r<=b<=outer_radius
      if (angle == Math.PI*2) {
          return {r: outer_radius, b: 0, outer_radius: outer_radius}
      } else {
          var R = angle/2,
              b = Math.sin(Math.PI/2) / Math.sin(R),
              r = outer_radius/(b+1);
          return {r: r, b: b*r};   
      }  
  } 

  function circlegroups (item_radius) {
      var cx = 0.5,
          cy = 0.5,
          radius = 0.5,
          item_radius = item_radius || 0;

      function layout (root) {
          /*
          Lays out the specified root hierarchy, assigning the following
          properties on root and its descendants:

          node.x - the x-coordinate of the circle’s center node.y - the
          y-coordinate of the circle’s center node.r - the radius of the
          circle
          */
          var dangle = Math.PI*2 / root.children.length,
              max_items = 0,
              max_value = 0,
              max_value_index=0,
              a = 0;

          // var root_value = root.children.length;

          // calc max_items
          for (var i=0, l=root.children.length; i<l; i++) {
              var g = root.children[i];
              if (g.children.length && g.children.length > max_items) {
                  max_items = g.children.length;
              }
              if (g.value > max_value) {
                  max_value = g.value;
                  max_value_index = i;
              }
          }
          // layout centered on current angle...
          // set the start angle to some fixed/home value for each group
          // layout starting with the active group

          console.log("circle.layout, root.value", root.value, "max_value_index", max_value_index, "max_value", max_value);
          var count = 0;
          var i = max_value_index;
          a = dangle * max_value_index;

          // for (var i=0, l=root.children.length; i<l; i++) {
          while (true) {
              var g = root.children[i],
                  g_angle = Math.PI*2 * (g.value / root.value),
                  d = arc_inscribe_circle(g_angle, radius);
              if (count > 0) { a += (g_angle / 2); }
              g.x = cx + (Math.cos(a) * d.b);
              g.y = cy - (Math.sin(a) * d.b);
              g.r = d.r;
              a += (g_angle/2);
              phyllotaxis(g.x, g.y, d.r, item_radius)(g.children, max_items);
              if (++i >= root.children.length) { i=0; }            if (++count >= root.children.length) break;
          }

          return layout;
      }

      function _radius (r) {
          if (r === undefined) {
              return radius;
          }
          that.radius = r;
          return layout;
      }

      function _size (s) {
          if (s === undefined) {
              return s;
          }
          cx = s[0]/2;
          cy = s[1]/2;
          radius = Math.min(cx, cy);
          // console.log("_size", cx, cy, radius);
          return layout;
      }

      layout.radius = _radius;
      layout.size = _size;
      return layout;
  }

  function flow_layout (sel, x, y, width, space_width) {
      // console.log("flow_layout", sel.size());
      space_width = space_width || 8;
      var initial_x = x,
          line_height = 0;
      sel.each(function(d) {
          var bbox = this.getBBox();
          line_height = Math.max(line_height, bbox.height);
          if ((x > initial_x) && ((x + bbox.width) >= width)) {
              // wrap
              x = initial_x;
              y += line_height;
              // line_height = 0;
              d.x = x;
              d.y = y;
              x += bbox.width + space_width;
          } else {
              d.x = x;
              d.y = y;
              x += bbox.width + space_width;
          }
      });
      if (x > initial_x) { y += line_height; }
      return y;
  }

  var FACET_KEYS = ['faculty_name', 'event_category', 'course_language'];
  var COLORS = ["rgb(110, 64, 170)","rgb(238, 67, 149)", "rgb(255, 140, 56)"];
  var LEFT_MARGIN_TEXT = 40;
  var LEFT_MARGIN_SYMBOLS = 10;
  var LEFT_WIDTH = 320;

  // import EventEmitter from 'eventemitter3';

  var COURSE_INFO_URL;

  var width,
      height,
      margin,
      left,
      top,
      right,
      bottom,
      layout = {},
      rtree$1 = lib(10),
      active_item = null,
      facets = null,
      active_facet = null,
      active_facet_index = null,
      highlight_facet_value = null,
      line_height = 11,
      svg = select("#content")
          .append("svg")
          .attr("xmlns", "http://www.w3.org/2000/svg")
          .attr("xmlns:xlink", "http://www.w3.org/1999/xlink"),
      debugcircles = svg.append("g").attr("id", "debugcircles"),
      base = svg.append("g").attr("class", "base"),
      // color_scaler = [],
      // COLORS = [],
      can_layout = false,
      debug_circle = 
          svg.append("circle").attr("id", "debug_circle").attr('class', 'debug'),
      debug_rect = 
          svg.append("rect").attr("id", "debug_rect").attr('class', 'debug'),
      textbox = select("body").append("div").attr("id", "textbox").style("display", "none"),
      tb_contents = textbox.append("div").attr("class", "contents"),
      tb_title = tb_contents.append("div").attr("class", "title").text("Title"),
      tb_body = tb_contents.append("div").attr("class", "body").text("BODY"),
      tb_link = tb_contents.append("div").attr("class", "link")
          .append("a")
          .attr("class", "link")
          .attr("target", "campusnet")
          .html("mehr lesen&hellip;"),
      facetbuttons = select("#facetbuttons"),
      item_radius = 10;  

  document.getElementById("title").style.left = LEFT_MARGIN_TEXT + "px";

  function resize () {
      var w = window,
          d = document,
          e = d.documentElement,
          g = d.getElementsByTagName('body')[0];

      width = w.innerWidth || e.clientWidth || g.clientWidth;
      height = w.innerHeight|| e.clientHeight|| g.clientHeight;
      margin = Math.max(0, Math.min(50, width * 0.05));
      left = margin;
      top = margin;
      right = width - margin;
      bottom = height - margin;    
      // console.log("mouse_box", mouse_box);

      svg.attr("width", width);
      svg.attr("height", height);

      // position the layout circle + rect
      if (!layout.circle) { layout.circle = {}; }    if (!layout.rect) { layout.rect = {}; }
      // center circle
      layout.circle.r = Math.min(width/2, height/2);
      item_radius = layout.circle.r / 18;
      console.log("item_radius", item_radius);
      // adjust text box sizes relative to item_radius
  /*
      padding-left: 20px;
      padding-right: 23px;
      padding-bottom: 18px;
      margin-top: -10px;
      margin-bottom: -18px;
  */
      var ir = Math.round(item_radius);
      tb_contents.style("padding-left", (ir+2)+"px");
      tb_contents.style("padding-right", (ir+5)+"px");
      tb_contents.style("margin-top", -(10)+"px");
      tb_contents.style("margin-bottom", -(ir-4)+"px");
      svg.selectAll("g.item circle").attr("r", item_radius);

      // layout.circle.x = (width - (2*layout.circle.r)) / 2;     // hcenter
      // layout.circle.x = (width - (2*layout.circle.r)); // right align

      // left edge of circle is either the left edge of the LEFT_WIDTH
      //    OR whatever is necessary to keep the right edge of the layout.circle on the screen (min)
      layout.circle.x = Math.min(LEFT_WIDTH, (width - (2*layout.circle.r)));

      // layout.circle.y = (height - (2*layout.circle.r)) / 2;
      // bottom align
      layout.circle.y = (height - (2*layout.circle.r));
      layout.circle.cx = layout.circle.x + layout.circle.r;
      layout.circle.cy = layout.circle.y + layout.circle.r;
      layout.rect.width = LEFT_WIDTH;
      layout.rect.height = height;
      layout.rect.x = 0;
      layout.rect.y = 0;


      debug_circle
          .attr("cx", layout.circle.cx)
          .attr("cy", layout.circle.cy)
          .attr("r", layout.circle.r);
      debug_rect
          .attr("x", layout.rect.x)
          .attr("y", layout.rect.y)
          .attr("width", layout.rect.width)
          .attr("height", layout.rect.height);
      update_layout();
  }
  resize();
  window.addEventListener("resize", resize);

  function resum_facet (facet) {
      // console.log("resum facet");
      // original
      // facet.sum(d => {
      //     // if (d.facet_parent_nodes) {
      //     //     var pnode = d.facet_parent_nodes[active_facet_index];
      //     //     return pnode.data.expanded ? 5*d.value : d.value;            
      //     // }
      //     // return d.value;
      //     if (d.facet_nodes) {
      //         var pnode = d.facet_nodes[active_facet_index].parent;
      //         return pnode.data.expanded ? 5*d.value : d.value;            
      //     }
      //     return d.value;
      // });    
      // test: constant per group -- only root node is sum
      facet.eachAfter (d => {
          if (d.depth == 0) {
              d.value = 0;
              for (var i=0, l=d.children.length; i<l; i++) {
                  d.value += d.children[i].value;
              }
          } else if (d.data.expanded) {
              d.value = 5;
          } else {
              d.value = 1;
          }
      });
  }

  function unexpand_group (g) {
      if (g === undefined) {
          // find the active / expanded facetvalue
          var elt = svg.select("g.facetlabel.expanded");
          // console.log("unexpand_group: elt", elt);
          if (elt.size() == 1) {
              g = elt.datum();
              // console.log("found expanded facetlabel", g);
          } else {
              // console.log("unexpand_group couldn't find g");
              return;
          }
      }
      var facet = g.parent;
      if (facet.parent !== null) {
          console.log("BAD THING, facet.parent is not null");
      }
      facet.data.expanded = false;
      g.data.expanded = false;
      resum_facet(facet);

      // sync css classes
      svg.selectAll("g.facetlabel").classed("expanded", d=>d.data.expanded);
      svg.selectAll("g.facet").classed("expanded", d=>d.data.expanded);
  }

  function expand_group (g) {
      // console.log("expand group", g, g.depth);
      var facet = g.parent;
      if (facet.parent !== null) { // in other words g.depth should be 1
          console.log("BAD THING, facet.parent is not null");
      }
      // prevent double...
      if (g.data.expanded) { return }
      facet.children.forEach(g => {
          g.data.expanded = false;
      });
      facet.data.expanded = true; // mark parent as expanded to allow css to gray out siblings (by virtue of being not expanded in an expanded context)
      g.data.expanded = true;

      resum_facet(facet);

      // facet.sum(d => d.values ? (d.expanded ? (d.values.length * 5) : d.values.length) : 1);
      // facet.sum(d => {
      //     if (d.facet_parent_nodes) {
      //         var pnode = d.facet_parent_nodes[active_facet_index];
      //         // console.log("sum node", pnode);
      //         return pnode.data.expanded ? 5*d.value : d.value;            
      //     }
      //     return d.value;
      // });

      // sync css classes
      svg.selectAll("g.facetlabel").classed("expanded", d=>d.data.expanded);
      svg.selectAll("g.facet").classed("expanded", d=>d.data.expanded);

      // console.log("expand_group", g);
  }

  function set_highlight_facet_value (f, time) {
      // console.log("highlight_facet_value", f);
      if (highlight_facet_value) {
          // cleanup old value
          highlight_facet_value.highlight = false;
          highlight_facet_value.children.forEach(c => {
              c.data.highlight = false;
          });
          base.classed("facet"+highlight_facet_value.parent.data.index, false);
      }
      highlight_facet_value = f;
      if (highlight_facet_value) {
          // f.parent.data.highlight_facet = f;
          highlight_facet_value.highlight = true;
          // console.log(`highlighting ${highlight_facet_value.children.length} nodes`);
          highlight_facet_value.children.forEach(function (c) {
              c.data.highlight = true;
          });
          base.classed("facet"+highlight_facet_value.parent.data.index, true);
      }
      // console.log(`adjusting ${svg.select("g#facets").selectAll("g.value").size()} values`);
      svg.select("g#facets").selectAll("g.facetlabel").classed("highlight", d=>d.highlight);
      svg.selectAll("g.item").classed("highlight", d=>d.highlight);
  }



  function resort() {
      var elements = base.selectAll("g.item");
      elements.sort(function (a, b) {
          return a.z < b.z ? -1 : a.z > b.z ? 1 : a.z >= b.z ? 0 : NaN;
      });        
  }

  function hide_textbox() {
      textbox.style("display", "none");
  }

  function facet_value_click (d) {
      // console.log("facet_value_click", d.data.key, "highlighted?", d.highlight);
      // is this the active facet ?
      if (d.parent === active_facet) {
          // console.log("ACTIVE FACET")
          if (!d.data.expanded) {
              expand_group(d);
              set_active_facet(d.parent);
              update_layout(500);
          }
      } else {
          // console.log("INACTIVE FACET");
          if (!d.highlight) {
              // highlight
              console.log("highlight");
              set_highlight_facet_value(d);
          } else {
              // toggle
              set_highlight_facet_value();

              // switch to this facet + expand ?!
              // set_active_facet(d.parent);
              // expand_group(d);
              // hide_textbox();
              // update_layout(3000);
          }
      }
  }

  function set_active_facet (facet) {
      active_facet = facet;
      svg.selectAll("g.facet").classed("active", d=> d==active_facet);
      active_facet_index = facets.indexOf(facet);    
  }

  function update_layout (time) {
      if (!can_layout) return;
      // var thepack = pack()
      var thepack = circlegroups()
          .size([layout.circle.r*2, layout.circle.r*2]);
          // .padding(20);
      thepack(active_facet);

      // DEBUGGING - viz the whole structure
      // console.log("active_facet", active_facet);
      
      /* LAYOUT CIRCLES!!! */
      var allcircles = active_facet.descendants();
      var dc = debugcircles.selectAll("circle.layout").data(allcircles);
      dc.exit().remove();
      dc = dc.enter()
          .append("circle")
          .attr("class", "layout")
          .on("click", layout_circle_click)
          .merge(dc);
      dc.attr("cx", d=>layout.circle.x + d.x)
          .attr("cy", d=>layout.circle.y + d.y)
          .attr("r", d=>d.r);
      
      var lnodes = active_facet.leaves();
      // transfer layout to actual (shared) data elements at hierarchy leaf nodes
      // console.log("adjust position", layout.circle.cx - layout.circle.r, layout.circle.cy - layout.circle.r);
      for (var i=0, l=lnodes.length; i<l; i++) {
          lnodes[i].data.x = layout.circle.x + lnodes[i].x;
          lnodes[i].data.y = layout.circle.y + lnodes[i].y;
          lnodes[i].data.dr = lnodes[i].r;
      }

      // LAYOUT ITEMS (rebuild rtree)
      // rtree = RTree(10);
      var sel = base.selectAll("g.item");
      if (time) { sel = sel.transition().duration(time); }
          sel.each(function (d) {
              // rtree.insert({x: d.x-3, y: d.y-3, w: 6, h: 6}, this);
              // console.log("PLACE TEXTBOX", d.x, d.y);
              if (d == active_item) {
                  textbox.style("left", d.x+"px");
                  textbox.style("top", d.y+"px");
              }
          })
          .attr("transform", d => "translate("+d.x+","+d.y+")");
          //.select("circle")
          //.attr("r", d=>d.dr);

      // LAYOUT ACTIVE FACET LABELS
      // console.log("$", svg.select(`g.facet${active_facet_index}`).size());
      var sel = svg.select(`g#facet${active_facet_index}`)
          .selectAll("g.facetlabel");
      if (time) { sel = sel.transition().duration(time); }
      sel
          .attr("transform", d => `translate(${layout.circle.x+d.x},${layout.circle.y+d.y-10})`)
          .select("text").attr("text-anchor", "middle");

      layout_inactive_facets(time);

      // if (active_item) {
      //     position_active_text(time);        
      // }

  }

  function layout_inactive_facets (time) {
      var LINE_HEIGHT = 20;
      var y = LINE_HEIGHT*2;
      for (var i=0, len=facets.length; i<len; i++) {
          var sel = svg.select(`g#facet${i}`),
              // rect = sel.select("rect"),
              button = facetbuttons.select(`div#facet${i}`),
              vals = sel.selectAll("g.facetlabel");
          // console.log("vals", vals.size())

          // rect.attr("x", (i !== active_facet_index) ? 0 : layout.circle.cx)
          //     .attr("y", (i !== active_facet_index) ? y : 0);
          if (i === active_facet_index) {
              button.style("left", LEFT_MARGIN_SYMBOLS+"px")
                  .style("top", "0px")
                  .style("display", "none");
          } else {
              button.style("left", LEFT_MARGIN_SYMBOLS+"px")
                  .style("top", y+"px")
                  .style("display", "block");
          } 
          if (i !== active_facet_index) {
              y = flow_layout(vals, layout.rect.x + LEFT_MARGIN_TEXT, y, layout.rect.width-LEFT_MARGIN_TEXT, 8);
              if (time) { vals = vals.transition().duration(time); }
              vals.attr("transform", d => `translate(${d.x},${d.y+line_height})`)
                  .select("text").attr("text-anchor", "left");
              y += LINE_HEIGHT;
          }
      }
  }


  var mouse_element = null,
      circle_layout_mouse = null;

  function read_more_link (course_id, course_data_id) {
      return `https://campusnet.merz-akademie.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=COURSEDETAILS&ARGUMENTS=-N000000000000001,-N000304,-N000000000000000,-N${course_id},-N${course_data_id}`;
  }

  function set_circle_layout_mouse (d) {
      if (circle_layout_mouse == d) {
          return;
      }
      // console.log("set_circle_layout_mouse", d);
      if (circle_layout_mouse) {
          // cleanup old
          rsetprop(circle_layout_mouse, 'mouse2', false);
          circle_layout_mouse.mouse = false;
          circle_layout_mouse.data.mouse = false;
          circle_layout_mouse.data.z = 0;
          if (!circle_layout_mouse.children) {
              // CLEANUP OLD ITEM
              // hide textbox unless it's active
              if (!active_item) {
                  textbox.style("display", "none");
              }
          }
      }
      // console.log("MOUSE d", d);
      circle_layout_mouse = d;
      rsetprop(d, 'mouse2', true);
      circle_layout_mouse.mouse = true;
      circle_layout_mouse.data.mouse = true;
      circle_layout_mouse.data.z = 1;
      if (!d.children) {
          // mouseover item
          // .data is ITEM
          // this kind of sucks ... but eventually should improve to allow
          // mouseover titles maybe WHILE an active text_box is displayed (need another title element)
          set_active_item(null);
          svg.selectAll("g.item").classed("active", d=>d.active);
          display_text_box(d.data);
      }
      update();
      resort();
  }

  function display_text_box(d) {
      var sx = d.x,
          sy = d.y;
      textbox.datum(d);
      textbox.style("display", "block");
      textbox.classed("active", false);
      textbox.select(".title").text(d.course_name);
      textbox.style("left", sx+"px");
      textbox.style("top", sy+"px");
      textbox.classed("top", sy < height / 2);                    
      textbox.classed("bottom", sy >= height / 2);                    
      textbox.classed("left", sx < width / 2);                    
      textbox.classed("right", sx >= width / 2);
      textbox.select("a.link").attr("href", read_more_link(d.course_id, d.course_data_id));    
  }

  function move () {
      var e = event,
          mx,
          my;
      if (e.changedTouches) {
          mx = e.changedTouches[0].pageX;
          my = e.changedTouches[0].pageY;
      } else {
          mx = event.x,
          my = event.y;
      }
      var elt = document.elementFromPoint(mx, my);
      // console.log("move", mx, my, elt);
      if (elt !== mouse_element) {
          mouse_element = elt;
          // console.log("mouse_element", elt);
          if (mouse_element) {
              // console.log("mouse_element", elt.nodeName, elt.classList);
              if (elt.nodeName == "circle") {
                  var d = select(elt).datum();
                  // if (d) { console.log("circle.d", elt, d); }
                  if (d && d.facet_nodes) {
                      // touching actual g.item circle
                      d = d.facet_nodes[active_facet_index];
                      //console.log("d.revdata", d);
                      set_circle_layout_mouse(d);
                  }
                  if (d && elt.classList.contains("layout")) {
                      // touching circle.layout
                      set_circle_layout_mouse(d);
                  }
              }
          }
      }
  }

  function layout_circle_click (d) {
      // console.log("layout_circle_click", d);
      if (!d.children) {
          item_click(d.data);
      } else if (d.depth == 1) {
          facet_value_click(d);
      } else {
          // console.log("background click");
          hide_textbox();
      }
  }


  function set_active_item (x) {
      if (active_item) { active_item.active = false; }
      active_item = x;
      if (active_item) { active_item.active = true; }
  }

  function item_click (d) {
      // console.log("item_click", d);
      var group = d.facet_nodes[active_facet_index].parent;
      if (group && !group.data.expanded) {
          expand_group(group);
          update_layout(500);
          // console.log("reclicking in 1 sec");
          setTimeout(function () {
              item_click(d);
          }, 1000);
          return;
      }

      set_active_item(d);
      // update visual state
      svg.selectAll("g.item").classed("active", d=>d.active);

      display_text_box(d);
      if (!d.full_description) {
          d.full_description = "&hellip;";
          // trigger an async load / refresh
          json(COURSE_INFO_URL+"?course_id="+d.course_id)
              .then(function (data) {
                  // console.log("courseInfos", data);
                  d.full_description = "&mdash;";
                  for (var i=0, l=data.length; i<l; i++) {
                      if (data[i].Name == "Inhalte und Ziele") {
                          d.full_description = data[i].Text;
                          break;
                      }
                  }
                  // conditional reload/refresh
                  //if (!circle_layout_mouse || circle_layout_mouse.data === d) {
                      // if circle_layout_mouse hasn't changed in the meantime
                  textbox.select(".body").html(d.full_description);                
                  // }
              });
      }    
      textbox.select(".body").html(d.full_description);
      textbox.classed("active", true);

      // var group = d.facet_parent_nodes[active_facet_index];
      // if (group && !group.data.expanded) {
      //     expand_group(group);
      //     update_layout(500);
      // }
  }

  function rsetprop (d, prop, val) {
      while (true) {
          d[prop] = val;
          if (!d.parent) break;
          d = d.parent;
      }
  }

  function update () {
      // sync data states with elements
      svg.selectAll("circle.layout").classed("mouse", d=>d.mouse);
      // svg.selectAll("g.facet").classed("mouse", d=>d.mouse);
      svg.selectAll("g.facetlabel").classed("mouse", d=>d.mouse);
      svg.selectAll("g.facetlabel").classed("mouse2", d=>d.mouse2);
      svg.selectAll("g.item").classed("mouse", d=>d.mouse);

  }

  /*
  function set_active_item (elt) {
      console.log("set_active_item", elt);
      if (active_item) {
          var $old = select(active_item),
              oldd = $old.datum();
          $old.classed("active", false);
          oldd.active = false;
          oldd.z = 0;
          update_item(active_item);
      }
      active_item = elt;
      if (active_item) {
          var $this = select(elt),
              d = $this.datum();
          // console.log("set_active_item", d.course_name);
          var group = d.facet_nodes[active_facet_index].parent;
          // console.log("item_group", group);
          if (group) {
              expand_group(group);
              update_layout(500);
              // set_active_facet(active_facet);
          }
          d.active = true;
          d.z = 1;
          $this.classed("active", true);
          update_item(elt);
          resort();
      }
  }
  */

  // function position_active_text (time) {
  //     var bbox = active_item.getBoundingClientRect();
  //     console.log("bbox", active_item, bbox);
  //     var sel = display_text;
  //     if (time) {
  //         sel = sel.transition().duration(time);
  //     }
  //     sel.attr("x", bbox.left)
  //         .attr("y", bbox.top + 30);
  //     sel.selectAll("tspan").attr("x", bbox.left).attr("y", bbox.top + 30);
  // }

  ///////////////////////////////////
  // INIT (1 time functions)
  ///////////////////////////////////

  function remove_duplicates (data, key) {
      var seen = {},
          ret = [];
      for (var i=0, len=data.length; i<len; i++) {
          var keyvalue = data[i][key];
          if (!seen[keyvalue]) {
              seen[keyvalue] = true;
              ret.push(data[i]);
          }
      }
      return ret;
  }

  /*

  The facets are d3 hierarchy (root) nodes.

  They have children & depth values.
  The associated data is a d3 nest object with key (facetlabel) & values

  g#facets > g.facet <==> d is d3.hierarchy based on created root based on the 3 facet TOP values
  g#facets > g.facet > g.facetlabel <==> d are d3.hierarchy nodes at the level of the facetvalue                 


  */

  function build_facets (facets) {
      // make a flat space of labels for each facet value
      // layout as part of the
      // console.log('facets', facets);
      for (var i=0, l=facets.length; i<l; i++) {
          var color = warm((1.0 / facets.length)*i);
          facets[i].data.index = i;
          // COLORS.push(color);
          // color_scaler.push(scaleLinear()
          //     .domain([0, 1])
          //     .range([gray, color]));
      }
      // console.log("COLORS", settings.COLORS);
      var group = svg.append("g").attr("id", "facets").selectAll("g.facet")
          .data(facets)
          .enter()
          .append("g")
          .attr("class", "facet")
          .attr("id", (d, i) => `facet${i}`);
          // .attr("transform", (d, i) => `translate(${i*100},${0})`);

      // group.append("rect")
      //     .attr("width", 12)
      //     .attr("height", 12)
      //     .attr("x", 0)
      //     // .attr("x", (d, i) => i*16)
      //     .attr("class", (d, i) => "facet")
      //     .attr("id", (d, i) => "facet"+i)
      //     .on("click", function (d) {
      //         // console.log("facet box click", d);
      //         if (d == active_facet) {
      //             // console.log("FACET ALREADY ACTIVE");
      //         } else {
      //             set_active_facet(d);
      //             hide_textbox();
      //             update_layout(3000);                
      //         }
      //     })

      facetbuttons.selectAll("div.facetbutton")
          .data(facets)
          .enter()
          .append("div")
          .attr("class", "facetbutton")
          .attr("id", (d, i) => `facet${i}`)
          .append("a")
          .style("color", (d, i) => COLORS[i])
          .attr("href", "#")
          .on("click", function (d) {
              event.preventDefault();
              console.log("facet button click", d);
              if (d == active_facet) ; else {
                  set_active_facet(d);
                  hide_textbox();
                  update_layout(3000);                
              }            
          })
          .append("img")
          .attr("src", "img/symbol.png");



          // .attr("fill", (d, i) => COLORS[i]);

      group.selectAll("g.facetlabel")
          .data(d => d.children)
          .enter()
          .append("g")
          .attr("class", "facetlabel")
          .append("a")
          .attr("xlink:href", "#")
          .on("click", function (d) {
              event.preventDefault();
              facet_value_click.call(this, d);
              // activate_facet(d.parent);
          })
          .append("text")
          .text(d => { return (d.data.key == "null") ? "Sonstige" : d.data.key.replace(/_/g, "/") });
  }

  function init_items (data) {
      // random initial positions
      for (var i=0, l=data.length; i<l; i++) {
          data[i].x = left + (Math.random() * (right-left));
          data[i].y = top + (Math.random() * (bottom-top));
          data[i].z = 0;
          data[i].size = 6;
          data[i].value = 10 + Math.random()*5;
      }

      var enter = base.selectAll("g.item")
          .data(data, d => d.course_id)
          .enter();
      // console.log("enter", enter.size());
      var g = enter.append("g")
          .classed("item", true)
          .attr("transform", d => `translate(${d.x},${d.y})`);

      g.on("click", item_click);

      g.append("circle")
          .attr("r", item_radius)
          .attr("x", 0)
          .attr("y", 0);

      // THE MOVE FUNCTION provides mouseenter/exit hooks
      // also using touch events
      svg.on("mousemove", move);
      svg.on("touchmove", move);
      svg.on("click", function () {
          if (event.target == svg.node()) {
              console.log("svg.background click");
              if (textbox.style("display") !== "none") {
                  hide_textbox();
              } else {
                  unexpand_group();
                  update_layout(500);
              }
          }
      });    
  }

  textbox.on("click", function () {
      var d = textbox.datum();
      // console.log("textbox click", d);
      if (d) {
          item_click(d);
      }
  });

  function init_facets (data, facet_names) {
      // facets are d3.hiearchy objects that nest the data according to particular property names
      var facets = facet_names.map(facet_name => hierarchy(
      {
          key: facet_name,
          values: nest()
                      .key(d => d[facet_name])
                      .entries(data)
      },
          d => d.values
      ));
      facets.forEach(resum_facet);
      return facets; // .sum(d => d.value)); // was d=>1
  }

  function reverse_index_facets (facets) {
      // adds a list to each leaf datum called facet_parent_nodex
      // which contains each of the containing hierarchy nodes for
      // each facet
      for (var f=0, flen=facets.length; f<flen; f++) {
          var facet = facets[f];
          for (var v=0, vlen=facet.children.length; v<vlen; v++) {
              var value = facet.children[v];
              for (var c=0, clen=value.children.length; c<clen; c++) {
                  var child = value.children[c];
                  if (f == 0) {
                      // child.data.facet_parent_nodes = [];
                      child.data.facet_nodes = [];
                  }
                  // child.data.facet_parent_nodes.push(value);
                  child.data.facet_nodes.push(child);
              }
          }
      }
      return facets;
  }

  async function load (src, info) {
      COURSE_INFO_URL = info;
      var data = await json(src),
          old_count = data.length;
      data = remove_duplicates(data, 'course_id');
      console.log("Loaded ", old_count, "original data items, reduced to", data.length, " items with unique course_id"); 

      init_items(data);

      facets = init_facets (data, FACET_KEYS );
      // console.log("FACETS", facets);
      reverse_index_facets(facets);
      build_facets(facets);
      set_active_facet(facets[0]);
      can_layout = true;
      update_layout();
  }

  exports.load = load;
  exports.remove_duplicates = remove_duplicates;

  return exports;

}({}));
