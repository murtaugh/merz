export var KEYS = [
  "add_rooms",
  "application_end",
  "application_procedure",
  "application_start",
  "appointment_type",
  "certificate_name",
  "change_to_listener",
  "course_catalog",
  "course_data_id",
  "course_id",
  "course_language",
  "course_mandatory",
  "course_name",
  "course_number",
  "courselevel",
  "credit_points",
  "day_of_week",
  "description",
  "duration_units",
  "edate",
  "event_category",
  "event_type",
  "event_type_id",
  "faculty_name",
  "fixed_split",
  "grade_system",
  "grade_system_exams",
  "hours_per_week",
  "instructors_string",
  "intranet",
  "listener_allowed",
  "location_name",
  "max_points",
  "max_students",
  "method_of_weighting",
  "min_students",
  "modify_date",
  "public",
  "release",
  "release_date",
  "semester_id",
  "semester_name",
  "semesterblock",
  "short_desc",
  "show_instructors_web",
  "small_groups",
  "split",
  "studienfuehrer_desc",
  "study_component_id",
  "time_from",
  "time_to",
  "timetable_date",
  "timetable_id",
  "total_hours",
  "week_of_year",
  "weekeven",
  "year"
];


export function build_display_text (d) {
  return `${d.course_number} ${d.add_rooms} ${d.course_language}
${d.semester_name}
${d.semesterblock} ${d.short_desc}
${d.appointment_type}
${d.instructors_string}
${d.description}`;
}

// function build_display_text (d) {
//     console.log("build_display_text", d);
//     var ret = "";
//     for (let i=0, l=KEYS.length; i<l; i++) {
//         let key = KEYS[i];
//         ret += key + ": " + d[key];
//         if (i+1 < l) {
//             ret += ", ";
//         }
//     }    
//     return ret;
// }
