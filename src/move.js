function update_item (elt) {
    var $d = select(elt),
        d = $d.datum();
    $d.select("circle").style('fill', function (d) { return color_scaler[active_facet_index](d.z) });
    $d.select("text")
        .style('fill', function (d) { return color_scaler[active_facet_index](d.z) })
        // .style("visibility", d.mouse ? "visible" : "hidden");
        .style("visibility", (d.z > 0) ? "visible" : "hidden");
}

function set_active_item (elt) {
    console.log("set_active_item", elt);
    if (active_item) {
        var $old = select(active_item),
            oldd = $old.datum();
        $old.classed("active", false);
        oldd.active = false;
        oldd.z = 0;
        update_item(active_item);
    }
    active_item = elt;
    if (active_item) {
        var $this = select(elt),
            d = $this.datum();
        // console.log("set_active_item", d.course_name);
        var group = d.facet_parent_nodes[active_facet_index];
        // console.log("item_group", group);
        if (group) {
            expand_group(group);
            update_layout(500);
            // set_active_facet(active_facet);
        }
        d.active = true;
        d.z = 1;
        $this.classed("active", true);
        update_item(elt);
        resort();

        var text = build_display_text(d);
        display_text.text(text);
        svgtextwrap(display_text, 300);
        position_active_text();
    }
}

function set_mouse_item (elt, d) {
    if (mouse_item) {
        var $old = select(mouse_item),
            oldd = $old.datum();
        $old.classed("mouse", false);
        oldd.mouse = false;
        // oldd.z = 0;
        update_item(mouse_item);
    }
    mouse_item = elt;
    if (mouse_item) {
        var $this = select(elt),
            d = d || $this.datum();
        d.mouse = true;
        $this.classed("mouse", true);
        update_item(elt);
    }
    // resort();    
}

function move () {
    var e = event;
    // console.log("move", e);
    var mx, my;
    if (e.changedTouches) {
        mx = e.changedTouches[0].pageX;
        my = e.changedTouches[0].pageY;
    } else {
        mx = event.x,
        my = event.y;
    }
    
    var objs = rtree.bbox(mx-mouse_hbox, my-mouse_hbox, mx+mouse_hbox, my+mouse_hbox);
    // console.log(`move ${objs.length} items`);
    for (var i=0, l=prev_mouse_objects.length; i<l; i++) {
        var d = select(prev_mouse_objects[i]).datum();
        d.z = d.active ? 1 : 0;
        update_item(prev_mouse_objects[i]);
    }
    // decorate with distance
    for (var i=0, l=objs.length; i<l; i++) {
        var d = select(objs[i]).datum(),
            di = dist(d.x, d.y, mx, my);
        d.md = di;
    }
    // objs.sort((a, b) => (b - a));
    objs.sort((a, b) => (select(a).datum().md - select(b).datum().md));
    // console.log("objs", objs.map(x=>select(x).datum().md));
    for (var i=0, l=objs.length; i<l; i++) {
        var d = select(objs[i]).datum(),
            di = d.md;
        if (i == 0) { set_mouse_item(objs[i], d); }
        // console.log("d", objs[i], d, di);
        if (di <= mouse_hbox) {
            di = 1 - (di/mouse_hbox);
            di *= di;
            d.z = di;
            update_item(objs[i]);
        } else {
            d.z = d.active ? 1: 0;
        }
        // console.log("objs[i].z", objs[i].z);
        // window.requestAnimationFrame(drawCanvas);
    }
    if (objs.length == 0 && mouse_item) {
        set_mouse_item();
    }

    prev_mouse_objects = objs;
    // console.log("move", d3.event.x);

    resort();

}