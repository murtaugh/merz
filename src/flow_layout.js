export function flow_layout (sel, x, y, width, space_width) {
    // console.log("flow_layout", sel.size());
    space_width = space_width || 8;
    var initial_x = x,
        line_height = 0;
    sel.each(function(d) {
        var bbox = this.getBBox();
        line_height = Math.max(line_height, bbox.height);
        if ((x > initial_x) && ((x + bbox.width) >= width)) {
            // wrap
            x = initial_x;
            y += line_height;
            // line_height = 0;
            d.x = x;
            d.y = y;
            x += bbox.width + space_width;
        } else {
            d.x = x;
            d.y = y;
            x += bbox.width + space_width;
        }
    })
    if (x > initial_x) { y += line_height; }
    return y;
}
