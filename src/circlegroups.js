
function phyllotaxis (x, y, outer_radius, item_radius) {
    // https://www.geeksforgeeks.org/algorithmic-botany-phyllotaxis-python/
    // https://observablehq.com/@mbostock/phyllotaxis

    function layout (items, numitems) {
        let l = items.length,
            a, r,
            da = Math.PI * (3 - Math.sqrt(5)); // (Math.PI * 2 * rings) / l,
        numitems = numitems || items.length;

        // layout with a spacing set to extend up to outer_radius
        // allow numitems to be provided that's maybe different from the actual
        // length of items

        // substracting the item_radius keeps the shapes inside the outer circle
        var spacing = (outer_radius-item_radius) / Math.sqrt(numitems+0.5);

        for (let i=0; i<l; i++) {
            a = i * da;
            r = spacing * Math.sqrt(i+0.5);
            items[i].x = x + (r*Math.cos(a));
            items[i].y = y - (r*Math.sin(a));
            items[i].r = item_radius;
            // a += da;
        }
    }
    return layout;
}

function arc_inscribe_circle (angle, outer_radius) {
    // takes an angle and outer_radius
    // and calculates:
    // b: the distance from the arc origin to the center of the
    //    inscribed circle
    // r: the radius of the circle
    // nb: r<=b<=outer_radius
    if (angle == Math.PI*2) {
        return {r: outer_radius, b: 0, outer_radius: outer_radius}
    } else {
        var R = angle/2,
            b = Math.sin(Math.PI/2) / Math.sin(R),
            r = outer_radius/(b+1);
        return {r: r, b: b*r};   
    }  
} 

export function circlegroups (item_radius) {
    var size = [1,1],
        cx = 0.5,
        cy = 0.5,
        radius = 0.5,
        item_radius = item_radius || 0;

    function layout (root) {
        /*
        Lays out the specified root hierarchy, assigning the following
        properties on root and its descendants:

        node.x - the x-coordinate of the circle’s center node.y - the
        y-coordinate of the circle’s center node.r - the radius of the
        circle
        */
        var dangle = Math.PI*2 / root.children.length,
            max_items = 0,
            max_value = 0,
            max_value_index=0,
            a = 0;

        // var root_value = root.children.length;

        // calc max_items
        for (var i=0, l=root.children.length; i<l; i++) {
            var g = root.children[i];
            if (g.children.length && g.children.length > max_items) {
                max_items = g.children.length;
            }
            if (g.value > max_value) {
                max_value = g.value;
                max_value_index = i;
            }
        }
        // layout centered on current angle...
        // set the start angle to some fixed/home value for each group
        // layout starting with the active group

        console.log("circle.layout, root.value", root.value, "max_value_index", max_value_index, "max_value", max_value);
        var count = 0;
        var i = max_value_index;
        a = dangle * max_value_index;

        // for (var i=0, l=root.children.length; i<l; i++) {
        while (true) {
            var g = root.children[i],
                g_initialAngle = dangle * i,
                g_angle = Math.PI*2 * (g.value / root.value),
                d = arc_inscribe_circle(g_angle, radius);
            if (count > 0) { a += (g_angle / 2); }
            g.x = cx + (Math.cos(a) * d.b);
            g.y = cy - (Math.sin(a) * d.b);
            g.r = d.r;
            a += (g_angle/2);
            phyllotaxis(g.x, g.y, d.r, item_radius)(g.children, max_items);
            if (++i >= root.children.length) { i=0 };
            if (++count >= root.children.length) break;
        }

        return layout;
    }

    function _radius (r) {
        if (r === undefined) {
            return radius;
        }
        that.radius = r;
        return layout;
    }

    function _size (s) {
        if (s === undefined) {
            return s;
        }
        size = s;
        cx = s[0]/2;
        cy = s[1]/2;
        radius = Math.min(cx, cy);
        // console.log("_size", cx, cy, radius);
        return layout;
    }

    layout.radius = _radius;
    layout.size = _size;
    return layout;
}