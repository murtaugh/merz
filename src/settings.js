export var FACET_KEYS = ['faculty_name', 'event_category', 'course_language'];
export var COLORS = ["rgb(110, 64, 170)","rgb(238, 67, 149)", "rgb(255, 140, 56)"];
export var LEFT_MARGIN_TEXT = 40;
export var LEFT_MARGIN_SYMBOLS = 10;
export var LEFT_WIDTH = 320;