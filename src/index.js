import 'd3-transition';
import { event, select, selectAll } from 'd3-selection';
import { nest, values, set, map, keys } from 'd3-collection';
import { json } from 'd3-fetch';
import { interpolateWarm } from 'd3-scale-chromatic';
import { scaleLinear } from 'd3-scale';
import { hierarchy, pack } from 'd3-hierarchy';

import RTree from 'rtree';

import { circlegroups } from './circlegroups.js';
import { flow_layout } from './flow_layout.js';
import { svgtextwrap } from './svgtextwrap.js';
import * as settings from './settings.js';
// import EventEmitter from 'eventemitter3';

var COURSE_INFO_URL;

var width,
    height,
    margin,
    left,
    top,
    right,
    bottom,
    mouse_box,
    mouse_hbox,
    layout = {},
    rtree = RTree(10),
    active_item = null,
    mouse_item = null,
    facets = null,
    active_facet = null,
    active_facet_index = null,
    highlight_facet_value = null,
    transition_time = 2000,
    line_height = 11,
    gray = "lightgray",
    svg = select("#content")
        .append("svg")
        .attr("xmlns", "http://www.w3.org/2000/svg")
        .attr("xmlns:xlink", "http://www.w3.org/1999/xlink"),
    debugcircles = svg.append("g").attr("id", "debugcircles"),
    base = svg.append("g").attr("class", "base"),
    prev_mouse_objects = [],
    // color_scaler = [],
    // COLORS = [],
    can_layout = false,
    debug_circle = 
        svg.append("circle").attr("id", "debug_circle").attr('class', 'debug'),
    debug_rect = 
        svg.append("rect").attr("id", "debug_rect").attr('class', 'debug'),
    textbox = select("body").append("div").attr("id", "textbox").style("display", "none"),
    tb_contents = textbox.append("div").attr("class", "contents"),
    tb_title = tb_contents.append("div").attr("class", "title").text("Title"),
    tb_body = tb_contents.append("div").attr("class", "body").text("BODY"),
    tb_link = tb_contents.append("div").attr("class", "link")
        .append("a")
        .attr("class", "link")
        .attr("target", "campusnet")
        .html("mehr lesen&hellip;"),
    facetbuttons = select("#facetbuttons"),
    item_radius = 10;  

document.getElementById("title").style.left = settings.LEFT_MARGIN_TEXT + "px";

function resize () {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0];

    width = w.innerWidth || e.clientWidth || g.clientWidth;
    height = w.innerHeight|| e.clientHeight|| g.clientHeight;
    margin = Math.max(0, Math.min(50, width * 0.05));
    left = margin;
    top = margin;
    right = width - margin;
    bottom = height - margin;    

    mouse_box = Math.max(10, Math.min(100, width*0.10));
    mouse_hbox = mouse_box/2;
    // console.log("mouse_box", mouse_box);

    svg.attr("width", width);
    svg.attr("height", height);

    // position the layout circle + rect
    if (!layout.circle) { layout.circle = {} };
    if (!layout.rect) { layout.rect = {} };

    // center circle
    layout.circle.r = Math.min(width/2, height/2);
    item_radius = layout.circle.r / 18;
    console.log("item_radius", item_radius);
    // adjust text box sizes relative to item_radius
/*
    padding-left: 20px;
    padding-right: 23px;
    padding-bottom: 18px;
    margin-top: -10px;
    margin-bottom: -18px;
*/
    var ir = Math.round(item_radius);
    tb_contents.style("padding-left", (ir+2)+"px");
    tb_contents.style("padding-right", (ir+5)+"px");
    tb_contents.style("margin-top", -(10)+"px");
    tb_contents.style("margin-bottom", -(ir-4)+"px");
    svg.selectAll("g.item circle").attr("r", item_radius);

    // layout.circle.x = (width - (2*layout.circle.r)) / 2;     // hcenter
    // layout.circle.x = (width - (2*layout.circle.r)); // right align

    // left edge of circle is either the left edge of the LEFT_WIDTH
    //    OR whatever is necessary to keep the right edge of the layout.circle on the screen (min)
    layout.circle.x = Math.min(settings.LEFT_WIDTH, (width - (2*layout.circle.r)));

    // layout.circle.y = (height - (2*layout.circle.r)) / 2;
    // bottom align
    layout.circle.y = (height - (2*layout.circle.r));
    layout.circle.cx = layout.circle.x + layout.circle.r;
    layout.circle.cy = layout.circle.y + layout.circle.r;
    layout.rect.width = settings.LEFT_WIDTH;
    layout.rect.height = height;
    layout.rect.x = 0;
    layout.rect.y = 0;


    debug_circle
        .attr("cx", layout.circle.cx)
        .attr("cy", layout.circle.cy)
        .attr("r", layout.circle.r);
    debug_rect
        .attr("x", layout.rect.x)
        .attr("y", layout.rect.y)
        .attr("width", layout.rect.width)
        .attr("height", layout.rect.height);
    update_layout();
}
resize()
window.addEventListener("resize", resize);

function resum_facet (facet) {
    // console.log("resum facet");
    // original
    // facet.sum(d => {
    //     // if (d.facet_parent_nodes) {
    //     //     var pnode = d.facet_parent_nodes[active_facet_index];
    //     //     return pnode.data.expanded ? 5*d.value : d.value;            
    //     // }
    //     // return d.value;
    //     if (d.facet_nodes) {
    //         var pnode = d.facet_nodes[active_facet_index].parent;
    //         return pnode.data.expanded ? 5*d.value : d.value;            
    //     }
    //     return d.value;
    // });    
    // test: constant per group -- only root node is sum
    facet.eachAfter (d => {
        if (d.depth == 0) {
            d.value = 0;
            for (var i=0, l=d.children.length; i<l; i++) {
                d.value += d.children[i].value;
            }
        } else if (d.data.expanded) {
            d.value = 5;
        } else {
            d.value = 1;
        }
    })
}

function unexpand_group (g) {
    if (g === undefined) {
        // find the active / expanded facetvalue
        var elt = svg.select("g.facetlabel.expanded");
        // console.log("unexpand_group: elt", elt);
        if (elt.size() == 1) {
            g = elt.datum();
            // console.log("found expanded facetlabel", g);
        } else {
            // console.log("unexpand_group couldn't find g");
            return;
        }
    }
    var facet = g.parent;
    if (facet.parent !== null) {
        console.log("BAD THING, facet.parent is not null");
    }
    facet.data.expanded = false;
    g.data.expanded = false;
    resum_facet(facet);

    // sync css classes
    svg.selectAll("g.facetlabel").classed("expanded", d=>d.data.expanded);
    svg.selectAll("g.facet").classed("expanded", d=>d.data.expanded);
}

function expand_group (g) {
    // console.log("expand group", g, g.depth);
    var facet = g.parent;
    if (facet.parent !== null) { // in other words g.depth should be 1
        console.log("BAD THING, facet.parent is not null");
    }
    // prevent double...
    if (g.data.expanded) { return }
    facet.children.forEach(g => {
        g.data.expanded = false;
    });
    facet.data.expanded = true; // mark parent as expanded to allow css to gray out siblings (by virtue of being not expanded in an expanded context)
    g.data.expanded = true;

    resum_facet(facet);

    // facet.sum(d => d.values ? (d.expanded ? (d.values.length * 5) : d.values.length) : 1);
    // facet.sum(d => {
    //     if (d.facet_parent_nodes) {
    //         var pnode = d.facet_parent_nodes[active_facet_index];
    //         // console.log("sum node", pnode);
    //         return pnode.data.expanded ? 5*d.value : d.value;            
    //     }
    //     return d.value;
    // });

    // sync css classes
    svg.selectAll("g.facetlabel").classed("expanded", d=>d.data.expanded);
    svg.selectAll("g.facet").classed("expanded", d=>d.data.expanded);

    // console.log("expand_group", g);
}

function set_highlight_facet_value (f, time) {
    // console.log("highlight_facet_value", f);
    if (highlight_facet_value) {
        // cleanup old value
        highlight_facet_value.highlight = false;
        highlight_facet_value.children.forEach(c => {
            c.data.highlight = false;
        });
        base.classed("facet"+highlight_facet_value.parent.data.index, false);
    }
    highlight_facet_value = f;
    if (highlight_facet_value) {
        // f.parent.data.highlight_facet = f;
        highlight_facet_value.highlight = true;
        // console.log(`highlighting ${highlight_facet_value.children.length} nodes`);
        highlight_facet_value.children.forEach(function (c) {
            c.data.highlight = true;
        });
        base.classed("facet"+highlight_facet_value.parent.data.index, true);
    }
    // console.log(`adjusting ${svg.select("g#facets").selectAll("g.value").size()} values`);
    svg.select("g#facets").selectAll("g.facetlabel").classed("highlight", d=>d.highlight);
    svg.selectAll("g.item").classed("highlight", d=>d.highlight);
}



function resort() {
    var elements = base.selectAll("g.item");
    elements.sort(function (a, b) {
        return a.z < b.z ? -1 : a.z > b.z ? 1 : a.z >= b.z ? 0 : NaN;
    });        
}

function hide_textbox() {
    textbox.style("display", "none");
}

function facet_value_click (d) {
    // console.log("facet_value_click", d.data.key, "highlighted?", d.highlight);
    // is this the active facet ?
    if (d.parent === active_facet) {
        // console.log("ACTIVE FACET")
        if (!d.data.expanded) {
            expand_group(d);
            set_active_facet(d.parent);
            update_layout(500);
        } else {
            // REMOVING THIS BEHAVIOUR AS ON MOBILE ITS REALLY ANNOYING
            // PUTTING THIS INSTEAD IN BACKGROUND TAP CODE
            // unexpand_group(d);
            // set_active_facet(d.parent);
            // update_layout();
        }
    } else {
        // console.log("INACTIVE FACET");
        if (!d.highlight) {
            // highlight
            console.log("highlight");
            set_highlight_facet_value(d);
        } else {
            // toggle
            set_highlight_facet_value();

            // switch to this facet + expand ?!
            // set_active_facet(d.parent);
            // expand_group(d);
            // hide_textbox();
            // update_layout(3000);
        }
    }
}

function set_active_facet (facet) {
    active_facet = facet;
    svg.selectAll("g.facet").classed("active", d=> d==active_facet);
    active_facet_index = facets.indexOf(facet);    
}

function update_layout (time) {
    if (!can_layout) return;
    // var thepack = pack()
    var thepack = circlegroups()
        .size([layout.circle.r*2, layout.circle.r*2]);
        // .padding(20);
    thepack(active_facet);

    // DEBUGGING - viz the whole structure
    // console.log("active_facet", active_facet);
    
    /* LAYOUT CIRCLES!!! */
    var allcircles = active_facet.descendants();
    var dc = debugcircles.selectAll("circle.layout").data(allcircles);
    dc.exit().remove();
    dc = dc.enter()
        .append("circle")
        .attr("class", "layout")
        .on("click", layout_circle_click)
        .merge(dc);
    dc.attr("cx", d=>layout.circle.x + d.x)
        .attr("cy", d=>layout.circle.y + d.y)
        .attr("r", d=>d.r);
    
    var lnodes = active_facet.leaves();
    // transfer layout to actual (shared) data elements at hierarchy leaf nodes
    // console.log("adjust position", layout.circle.cx - layout.circle.r, layout.circle.cy - layout.circle.r);
    for (var i=0, l=lnodes.length; i<l; i++) {
        lnodes[i].data.x = layout.circle.x + lnodes[i].x;
        lnodes[i].data.y = layout.circle.y + lnodes[i].y;
        lnodes[i].data.dr = lnodes[i].r;
    }

    // LAYOUT ITEMS (rebuild rtree)
    // rtree = RTree(10);
    var sel = base.selectAll("g.item");
    if (time) { sel = sel.transition().duration(time); }
        sel.each(function (d) {
            // rtree.insert({x: d.x-3, y: d.y-3, w: 6, h: 6}, this);
            // console.log("PLACE TEXTBOX", d.x, d.y);
            if (d == active_item) {
                textbox.style("left", d.x+"px");
                textbox.style("top", d.y+"px");
            }
        })
        .attr("transform", d => "translate("+d.x+","+d.y+")");
        //.select("circle")
        //.attr("r", d=>d.dr);

    // LAYOUT ACTIVE FACET LABELS
    // console.log("$", svg.select(`g.facet${active_facet_index}`).size());
    var sel = svg.select(`g#facet${active_facet_index}`)
        .selectAll("g.facetlabel");
    if (time) { sel = sel.transition().duration(time); }
    sel
        .attr("transform", d => `translate(${layout.circle.x+d.x},${layout.circle.y+d.y-10})`)
        .select("text").attr("text-anchor", "middle");

    layout_inactive_facets(time);

    // if (active_item) {
    //     position_active_text(time);        
    // }

}

function layout_inactive_facets (time) {
    var LINE_HEIGHT = 20;
    var y = LINE_HEIGHT*2;
    for (var i=0, len=facets.length; i<len; i++) {
        var sel = svg.select(`g#facet${i}`),
            // rect = sel.select("rect"),
            button = facetbuttons.select(`div#facet${i}`),
            vals = sel.selectAll("g.facetlabel");
        // console.log("vals", vals.size())

        // rect.attr("x", (i !== active_facet_index) ? 0 : layout.circle.cx)
        //     .attr("y", (i !== active_facet_index) ? y : 0);
        if (i === active_facet_index) {
            button.style("left", settings.LEFT_MARGIN_SYMBOLS+"px")
                .style("top", "0px")
                .style("display", "none");
        } else {
            button.style("left", settings.LEFT_MARGIN_SYMBOLS+"px")
                .style("top", y+"px")
                .style("display", "block");
        } 
        if (i !== active_facet_index) {
            y = flow_layout(vals, layout.rect.x + settings.LEFT_MARGIN_TEXT, y, layout.rect.width-settings.LEFT_MARGIN_TEXT, 8);
            if (time) { vals = vals.transition().duration(time); }
            vals.attr("transform", d => `translate(${d.x},${d.y+line_height})`)
                .select("text").attr("text-anchor", "left");
            y += LINE_HEIGHT;
        }
    }
}


var mouse_element = null,
    circle_layout_mouse = null;

function read_more_link (course_id, course_data_id) {
    return `https://campusnet.merz-akademie.de/scripts/mgrqispi.dll?APPNAME=CampusNet&PRGNAME=COURSEDETAILS&ARGUMENTS=-N000000000000001,-N000304,-N000000000000000,-N${course_id},-N${course_data_id}`;
}

function set_circle_layout_mouse (d) {
    if (circle_layout_mouse == d) {
        return;
    }
    // console.log("set_circle_layout_mouse", d);
    if (circle_layout_mouse) {
        // cleanup old
        rsetprop(circle_layout_mouse, 'mouse2', false);
        circle_layout_mouse.mouse = false;
        circle_layout_mouse.data.mouse = false;
        circle_layout_mouse.data.z = 0;
        if (!circle_layout_mouse.children) {
            // CLEANUP OLD ITEM
            // hide textbox unless it's active
            if (!active_item) {
                textbox.style("display", "none");
            }
        }
    }
    // console.log("MOUSE d", d);
    circle_layout_mouse = d;
    rsetprop(d, 'mouse2', true);
    circle_layout_mouse.mouse = true;
    circle_layout_mouse.data.mouse = true;
    circle_layout_mouse.data.z = 1;
    if (!d.children) {
        // mouseover item
        // .data is ITEM
        // this kind of sucks ... but eventually should improve to allow
        // mouseover titles maybe WHILE an active text_box is displayed (need another title element)
        set_active_item(null);
        svg.selectAll("g.item").classed("active", d=>d.active);
        display_text_box(d.data);
    }
    update();
    resort();
}

function display_text_box(d) {
    var sx = d.x,
        sy = d.y;
    textbox.datum(d);
    textbox.style("display", "block");
    textbox.classed("active", false);
    textbox.select(".title").text(d.course_name);
    textbox.style("left", sx+"px");
    textbox.style("top", sy+"px");
    textbox.classed("top", sy < height / 2);                    
    textbox.classed("bottom", sy >= height / 2);                    
    textbox.classed("left", sx < width / 2);                    
    textbox.classed("right", sx >= width / 2);
    textbox.select("a.link").attr("href", read_more_link(d.course_id, d.course_data_id));    
}

function move () {
    var e = event,
        mx,
        my;
    if (e.changedTouches) {
        mx = e.changedTouches[0].pageX;
        my = e.changedTouches[0].pageY;
    } else {
        mx = event.x,
        my = event.y;
    }
    var elt = document.elementFromPoint(mx, my);
    // console.log("move", mx, my, elt);
    if (elt !== mouse_element) {
        mouse_element = elt;
        // console.log("mouse_element", elt);
        if (mouse_element) {
            // console.log("mouse_element", elt.nodeName, elt.classList);
            if (elt.nodeName == "circle") {
                var d = select(elt).datum();
                // if (d) { console.log("circle.d", elt, d); }
                if (d && d.facet_nodes) {
                    // touching actual g.item circle
                    d = d.facet_nodes[active_facet_index];
                    //console.log("d.revdata", d);
                    set_circle_layout_mouse(d);
                }
                if (d && elt.classList.contains("layout")) {
                    // touching circle.layout
                    set_circle_layout_mouse(d);
                }
            }
        }
    }
}

function layout_circle_click (d) {
    // console.log("layout_circle_click", d);
    if (!d.children) {
        item_click(d.data);
    } else if (d.depth == 1) {
        facet_value_click(d);
    } else {
        // console.log("background click");
        hide_textbox();
    }
}


function set_active_item (x) {
    if (active_item) { active_item.active = false; }
    active_item = x;
    if (active_item) { active_item.active = true; }
}

function item_click (d) {
    // console.log("item_click", d);
    var group = d.facet_nodes[active_facet_index].parent;
    if (group && !group.data.expanded) {
        expand_group(group);
        update_layout(500);
        // console.log("reclicking in 1 sec");
        setTimeout(function () {
            item_click(d);
        }, 1000);
        return;
    }

    set_active_item(d);
    // update visual state
    svg.selectAll("g.item").classed("active", d=>d.active);

    display_text_box(d);
    if (!d.full_description) {
        d.full_description = "&hellip;"
        // trigger an async load / refresh
        json(COURSE_INFO_URL+"?course_id="+d.course_id)
            .then(function (data) {
                // console.log("courseInfos", data);
                d.full_description = "&mdash;";
                for (var i=0, l=data.length; i<l; i++) {
                    if (data[i].Name == "Inhalte und Ziele") {
                        d.full_description = data[i].Text;
                        break;
                    }
                }
                // conditional reload/refresh
                //if (!circle_layout_mouse || circle_layout_mouse.data === d) {
                    // if circle_layout_mouse hasn't changed in the meantime
                textbox.select(".body").html(d.full_description);                
                // }
            });
    }    
    textbox.select(".body").html(d.full_description);
    textbox.classed("active", true);

    // var group = d.facet_parent_nodes[active_facet_index];
    // if (group && !group.data.expanded) {
    //     expand_group(group);
    //     update_layout(500);
    // }
}

function rsetprop (d, prop, val) {
    while (true) {
        d[prop] = val;
        if (!d.parent) break;
        d = d.parent;
    }
}

function update () {
    // sync data states with elements
    svg.selectAll("circle.layout").classed("mouse", d=>d.mouse);
    // svg.selectAll("g.facet").classed("mouse", d=>d.mouse);
    svg.selectAll("g.facetlabel").classed("mouse", d=>d.mouse);
    svg.selectAll("g.facetlabel").classed("mouse2", d=>d.mouse2);
    svg.selectAll("g.item").classed("mouse", d=>d.mouse);

}

/*
function set_active_item (elt) {
    console.log("set_active_item", elt);
    if (active_item) {
        var $old = select(active_item),
            oldd = $old.datum();
        $old.classed("active", false);
        oldd.active = false;
        oldd.z = 0;
        update_item(active_item);
    }
    active_item = elt;
    if (active_item) {
        var $this = select(elt),
            d = $this.datum();
        // console.log("set_active_item", d.course_name);
        var group = d.facet_nodes[active_facet_index].parent;
        // console.log("item_group", group);
        if (group) {
            expand_group(group);
            update_layout(500);
            // set_active_facet(active_facet);
        }
        d.active = true;
        d.z = 1;
        $this.classed("active", true);
        update_item(elt);
        resort();
    }
}
*/

// function position_active_text (time) {
//     var bbox = active_item.getBoundingClientRect();
//     console.log("bbox", active_item, bbox);
//     var sel = display_text;
//     if (time) {
//         sel = sel.transition().duration(time);
//     }
//     sel.attr("x", bbox.left)
//         .attr("y", bbox.top + 30);
//     sel.selectAll("tspan").attr("x", bbox.left).attr("y", bbox.top + 30);
// }

///////////////////////////////////
// INIT (1 time functions)
///////////////////////////////////

export function remove_duplicates (data, key) {
    var seen = {},
        ret = [];
    for (var i=0, len=data.length; i<len; i++) {
        var keyvalue = data[i][key];
        if (!seen[keyvalue]) {
            seen[keyvalue] = true;
            ret.push(data[i]);
        }
    }
    return ret;
}

/*

The facets are d3 hierarchy (root) nodes.

They have children & depth values.
The associated data is a d3 nest object with key (facetlabel) & values

g#facets > g.facet <==> d is d3.hierarchy based on created root based on the 3 facet TOP values
g#facets > g.facet > g.facetlabel <==> d are d3.hierarchy nodes at the level of the facetvalue                 


*/

function build_facets (facets) {
    // make a flat space of labels for each facet value
    // layout as part of the
    // console.log('facets', facets);
    for (var i=0, l=facets.length; i<l; i++) {
        var color = interpolateWarm((1.0 / facets.length)*i);
        facets[i].data.index = i;
        // COLORS.push(color);
        // color_scaler.push(scaleLinear()
        //     .domain([0, 1])
        //     .range([gray, color]));
    }
    // console.log("COLORS", settings.COLORS);
    var group = svg.append("g").attr("id", "facets").selectAll("g.facet")
        .data(facets)
        .enter()
        .append("g")
        .attr("class", "facet")
        .attr("id", (d, i) => `facet${i}`);
        // .attr("transform", (d, i) => `translate(${i*100},${0})`);

    // group.append("rect")
    //     .attr("width", 12)
    //     .attr("height", 12)
    //     .attr("x", 0)
    //     // .attr("x", (d, i) => i*16)
    //     .attr("class", (d, i) => "facet")
    //     .attr("id", (d, i) => "facet"+i)
    //     .on("click", function (d) {
    //         // console.log("facet box click", d);
    //         if (d == active_facet) {
    //             // console.log("FACET ALREADY ACTIVE");
    //         } else {
    //             set_active_facet(d);
    //             hide_textbox();
    //             update_layout(3000);                
    //         }
    //     })

    facetbuttons.selectAll("div.facetbutton")
        .data(facets)
        .enter()
        .append("div")
        .attr("class", "facetbutton")
        .attr("id", (d, i) => `facet${i}`)
        .append("a")
        .style("color", (d, i) => settings.COLORS[i])
        .attr("href", "#")
        .on("click", function (d) {
            event.preventDefault();
            console.log("facet button click", d);
            if (d == active_facet) {
                // console.log("FACET ALREADY ACTIVE");
            } else {
                set_active_facet(d);
                hide_textbox();
                update_layout(3000);                
            }            
        })
        .append("img")
        .attr("src", "img/symbol.png");



        // .attr("fill", (d, i) => COLORS[i]);

    group.selectAll("g.facetlabel")
        .data(d => d.children)
        .enter()
        .append("g")
        .attr("class", "facetlabel")
        .append("a")
        .attr("xlink:href", "#")
        .on("click", function (d) {
            event.preventDefault();
            facet_value_click.call(this, d);
            // activate_facet(d.parent);
        })
        .append("text")
        .text(d => { return (d.data.key == "null") ? "Sonstige" : d.data.key.replace(/_/g, "/") });
}

function init_items (data) {
    // random initial positions
    for (var i=0, l=data.length; i<l; i++) {
        data[i].x = left + (Math.random() * (right-left));
        data[i].y = top + (Math.random() * (bottom-top));
        data[i].z = 0;
        data[i].size = 6;
        data[i].value = 10 + Math.random()*5;
    }

    var enter = base.selectAll("g.item")
        .data(data, d => d.course_id)
        .enter();
    // console.log("enter", enter.size());
    var g = enter.append("g")
        .classed("item", true)
        .attr("transform", d => `translate(${d.x},${d.y})`);

    g.on("click", item_click);

    g.append("circle")
        .attr("r", item_radius)
        .attr("x", 0)
        .attr("y", 0);

    // THE MOVE FUNCTION provides mouseenter/exit hooks
    // also using touch events
    svg.on("mousemove", move);
    svg.on("touchmove", move);
    svg.on("click", function () {
        if (event.target == svg.node()) {
            console.log("svg.background click");
            if (textbox.style("display") !== "none") {
                hide_textbox();
            } else {
                unexpand_group();
                update_layout(500);
            }
        }
    })    
}

textbox.on("click", function () {
    var d = textbox.datum();
    // console.log("textbox click", d);
    if (d) {
        item_click(d);
    }
})

function init_facets (data, facet_names) {
    // facets are d3.hiearchy objects that nest the data according to particular property names
    var facets = facet_names.map(facet_name => hierarchy(
    {
        key: facet_name,
        values: nest()
                    .key(d => d[facet_name])
                    .entries(data)
    },
        d => d.values
    ));
    facets.forEach(resum_facet);
    return facets; // .sum(d => d.value)); // was d=>1
}

function reverse_index_facets (facets) {
    // adds a list to each leaf datum called facet_parent_nodex
    // which contains each of the containing hierarchy nodes for
    // each facet
    for (var f=0, flen=facets.length; f<flen; f++) {
        var facet = facets[f];
        for (var v=0, vlen=facet.children.length; v<vlen; v++) {
            var value = facet.children[v];
            for (var c=0, clen=value.children.length; c<clen; c++) {
                var child = value.children[c];
                if (f == 0) {
                    // child.data.facet_parent_nodes = [];
                    child.data.facet_nodes = [];
                }
                // child.data.facet_parent_nodes.push(value);
                child.data.facet_nodes.push(child);
            }
        }
    }
    return facets;
}

export async function load (src, info) {
    COURSE_INFO_URL = info;
    var data = await json(src),
        old_count = data.length;
    data = remove_duplicates(data, 'course_id');
    console.log("Loaded ", old_count, "original data items, reduced to", data.length, " items with unique course_id"); 

    init_items(data);

    facets = init_facets (data, settings.FACET_KEYS );
    // console.log("FACETS", facets);
    reverse_index_facets(facets);
    build_facets(facets);
    set_active_facet(facets[0]);
    can_layout = true;
    update_layout();
}

